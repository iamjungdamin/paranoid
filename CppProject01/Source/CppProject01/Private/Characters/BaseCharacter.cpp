// Fill out your copyright notice in the Description page of Project Settings.


#include "Characters/BaseCharacter.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Animation/AnimMontage.h"
#include "Net/UnrealNetwork.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"

#include "Items/BaseWeapon.h"
#include "Items/FireSword.h"
#include "Items/IceSword.h"
#include "Items/ElectricSword.h"
#include "Pickups/BaseCrystal.h"

#include "GameData/P_GameInstance.h"
#include "GameData/P_PlayerController.h"
#include "Camera/PlayerCameraManager.h"
#include "Characters/Widget_Character.h"
#include "GameData/Door.h"
#include "GameData/Portal.h"

// Sets default values
ABaseCharacter::ABaseCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetMesh()->SetRelativeLocation(FVector(0.f, 0.f, -90.f));
	GetMesh()->SetRelativeRotation(FRotator(0.f, -90.f, 0.f));
	GetMesh()->bRenderCustomDepth = true;
	GetMesh()->CustomDepthStencilValue = 2;

	GetCharacterMovement()->JumpZVelocity = 380.f;

	GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &ABaseCharacter::OnOverlapBegin);
	GetCapsuleComponent()->OnComponentEndOverlap.AddDynamic(this, &ABaseCharacter::OnOverlapEnd);

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	CameraBoom->SetupAttachment(GetRootComponent());
	CameraBoom->TargetArmLength = 500.f;
	CameraBoom->bUsePawnControlRotation = true;
	CameraBoom->bInheritPitch = true;
	CameraBoom->bInheritYaw = true;
	CameraBoom->bInheritRoll = true;
	CameraBoom->SetRelativeLocationAndRotation(FVector(0.f, 0.f, 90.f), FRotator(0.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false;

	ViewCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("ViewCamera"));
	ViewCamera->SetupAttachment(CameraBoom);

	ConstructorHelpers::FClassFinder<UCameraShakeBase>CameraShakeAttackAsset(TEXT("/Game/Blueprints/GameData/CS_Attack.CS_Attack_C"));
	if (CameraShakeAttackAsset.Succeeded()) {
		CS_Attack = CameraShakeAttackAsset.Class;
	}
}

// Called when the game starts or when spawned
void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = true;

	PlayerController = Cast<AP_PlayerController>(GetController());
	if (PlayerController) {
		PlayerController->SetInputMode(FInputModeGameOnly());
		PlayerController->PlayerCameraManager->ViewPitchMin = -90.f;
		PlayerController->PlayerCameraManager->ViewPitchMax = 20.f;
	}

	maxHp = currentHp;
}

void ABaseCharacter::MoveForward(float Value)
{
	if (!isIdle) {
		return;
	}

	if (Controller && (Value != 0.f)) {
		//FVector Forward = GetActorForwardVector();
		//AddMovementInput(Forward, Value);

		// find out which way is forward
		const FRotator ControlRotation = GetControlRotation();
		const FRotator YawRotation{ 0.f, ControlRotation.Yaw, 0.f };

		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ABaseCharacter::MoveRight(float Value)
{
	if (!isIdle) {
		return;
	}

	if (Controller && (Value != 0.f)) {
		//FVector Right = GetActorRightVector();
		//AddMovementInput(Right, Value);

		// find out which way is right
		const FRotator ControlRotation = GetControlRotation();
		const FRotator YawRotation{ 0.f, ControlRotation.Yaw, 0.f };

		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(Direction, Value);
	}
}

void ABaseCharacter::Turn(float Value)
{
	AddControllerYawInput(Value);
}

void ABaseCharacter::LookUp(float Value)
{
	AddControllerPitchInput(Value);
}

void ABaseCharacter::Jump()
{
	Super::Jump();

	isJumping = true;

	FTimerHandle TimerHandle;
	float delay = 0.1f;
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, [=]()
		{
			isJumping = false;
		}, delay, false);
}

// Called every frame
void ABaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ABaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(FName("MoveForward"), this, &ABaseCharacter::MoveForward);
	PlayerInputComponent->BindAxis(FName("MoveRight"), this, &ABaseCharacter::MoveRight);
	PlayerInputComponent->BindAxis(FName("Turn"), this, &ABaseCharacter::Turn);
	PlayerInputComponent->BindAxis(FName("LookUp"), this, &ABaseCharacter::LookUp);

	PlayerInputComponent->BindAction(FName("Jump"), IE_Pressed, this, &ABaseCharacter::Jump);
	PlayerInputComponent->BindAction(FName("Interact"), IE_Pressed, this, &ABaseCharacter::SetIsInteractingToTrue);
	PlayerInputComponent->BindAction(FName("Interact"), IE_Released, this, &ABaseCharacter::SetIsInteractingToFalse);
	
	PlayerInputComponent->BindAction(FName("ChangeWeapon"), IE_Pressed, this, &ABaseCharacter::ChangeWeapon);
	PlayerInputComponent->BindAction(FName("Dash"), IE_Pressed, this, &ABaseCharacter::Dash);

	PlayerInputComponent->BindAction(FName("LeftClick"), IE_Pressed, this, &ABaseCharacter::ComboAttack);
	PlayerInputComponent->BindAction(FName("RightClick"), IE_Pressed, this, &ABaseCharacter::Charge);
	PlayerInputComponent->BindAction(FName("RightClick"), IE_Released, this, &ABaseCharacter::ChargedAttack);
	PlayerInputComponent->BindAction(FName("One"), IE_Pressed, this, &ABaseCharacter::Skill01);
	PlayerInputComponent->BindAction(FName("Two"), IE_Pressed, this, &ABaseCharacter::Skill02);
	PlayerInputComponent->BindAction(FName("Three"), IE_Pressed, this, &ABaseCharacter::Skill03);

}

void ABaseCharacter::SetMontages(FString folderPath)
{
	TArray<FString> FileNames = {
		"/AM_Dash",

		"/AM_FireBasicAttack",
		"/AM_IceBasicAttack",
		"/AM_ElectricBasicAttack",

		"/AM_FireChargedAttack",
		"/AM_IceChargedAttack",
		"/AM_ElectricChargedAttack",

		"/AM_FireSkill",
		"/AM_IceSkill",
		"/AM_ElectricSkill",

		"/AM_Hit"
	};

	for (int i = 0; i < FileNames.Num(); ++i) {
		FString Path = folderPath + FileNames[i];
		ConstructorHelpers::FObjectFinder<UAnimMontage>MontageAsset(*Path);
		
		if (MontageAsset.Succeeded()) {
			if (i == 0) {
				DashMontage = MontageAsset.Object;
			}
			else if (i < 4) {
				BasicAttackMontages[i - 1] = MontageAsset.Object;
			}
			else if (i < 7) {
				ChargedAttackMontages[i - 4] = MontageAsset.Object;
			}
			else if (i < 10) {
				SkillMontages[i - 7] = MontageAsset.Object;
			}
			else if (i == 10) {
				HitMontage = MontageAsset.Object;
			}
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
			continue;
		}
	}
}

void ABaseCharacter::ChangeWeapon()
{
	if (!isIdle) {
		return;
	}

	WeaponIndex = (WeaponIndex + 1) % 3;
	SetWeapon(WeaponIndex);
	PlayerController->GetWidget()->UpdateWeapon(this);
}

void ABaseCharacter::Dash()
{
	if (!isIdle) {
		return;
	}

	FVector Forward = GetActorForwardVector() * 20000.f;
	FVector Down = FVector(0, 0, -300.f);
	LaunchCharacter(Forward + Down, true, true);

	PlayMontage(DashMontage);
}

void ABaseCharacter::ComboAttack()
{
	if (!isIdle) {
		comboUpdate = true;
		return;
	}

	BasicAttack();
}

void ABaseCharacter::BasicAttack()
{
	FName SectionName = FName();
	if (comboCount == 0) {
		SectionName = FName("01");
	}
	else if (comboCount == 1) {
		SectionName = FName("02");
	}
	else if (comboCount == 2) {
		SectionName = FName("03");
	}
	else if (comboCount == 3) {
		SectionName = FName("04");
	}
	else if (comboCount == 4) {
		SectionName = FName("05");
	}

	isIdle = false;
	PlayMontage(BasicAttackMontages[WeaponIndex], SectionName);
}

void ABaseCharacter::Charge()
{
	if (!isIdle) {
		return;
	}

	isIdle = false;
	PlayMontage(ChargedAttackMontages[WeaponIndex], "01");
}

void ABaseCharacter::ChargedAttack()
{
	PlayMontage(ChargedAttackMontages[WeaponIndex], "02");
}

void ABaseCharacter::Skill01()
{
	if (!isIdle) {
		return;
	}

	isIdle = false;
	PlayMontage(SkillMontages[WeaponIndex], "01");
}

void ABaseCharacter::Skill02()
{
	if (!isIdle) {
		return;
	}

	isIdle = false;
	PlayMontage(SkillMontages[WeaponIndex], "02");
}

void ABaseCharacter::Skill03()
{
	if (!isIdle) {
		return;
	}

	isIdle = false;
	PlayMontage(SkillMontages[WeaponIndex], "03");
}

void ABaseCharacter::PlayMontage(UAnimMontage* Montage, FName SectionName)
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AnimInstance && Montage) {
		AnimInstance->Montage_Play(Montage);

		if (SectionName != NAME_None) {
			AnimInstance->Montage_JumpToSection(SectionName, Montage);
		}
	}
}

void ABaseCharacter::OnOverlapBegin(UPrimitiveComponent* Comp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp) {
		ABaseCrystal* Crystal = Cast<ABaseCrystal>(OtherActor);
		if (Crystal) {
			Crystal->SetAttracterLocation(GetActorLocation());
			Crystal->SetIsAttracted(true);
			++crystalCount[Crystal->GetCrystalType()];
			PlayerController->GetWidget()->UpdateCrystalCount(this);
			return;
		}

		ADoor* Door = Cast<ADoor>(OtherActor);
		if (Door) {
			Door->SetOwner(this);
			return;
		}
	}
}

void ABaseCharacter::OnOverlapEnd(UPrimitiveComponent* Comp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && (OtherActor != this) && OtherComp) {
		ABaseCrystal* Crystal = Cast<ABaseCrystal>(OtherActor);
		if (Crystal) {
			Crystal->SetIsAttracted(false);
			return;
		}

		ADoor* Door = Cast<ADoor>(OtherActor);
		if (Door) {
			Door->SetOwner(nullptr);
			return;
		}
	}
}

void ABaseCharacter::OpenPortal()
{
	GetWorld()->SpawnActor<APortal>(GetActorLocation(), GetActorRotation());
}

void ABaseCharacter::SetWeapon(int value)
{
	if (Weapon) {
		Weapon->OnUnequipped();
		Weapon->Destroy();
	}

	WeaponIndex = value;

	if (WeaponIndex == 0) {
		Weapon = GetWorld()->SpawnActor<AFireSword>(GetActorLocation(), GetActorRotation());
		Weapon->SetOwner(Cast<AActor>(this));
		Weapon->SetInstigator(Cast<APawn>(this));

		Weapon->OnEquipped();
	}
	else if (WeaponIndex == 1) {
		Weapon = GetWorld()->SpawnActor<AIceSword>(GetActorLocation(), GetActorRotation());
		Weapon->SetOwner(Cast<AActor>(this));
		Weapon->SetInstigator(Cast<APawn>(this));

		Weapon->OnEquipped();
	}
	else if (WeaponIndex == 2) {
		Weapon = GetWorld()->SpawnActor<AElectricSword>(GetActorLocation(), GetActorRotation());
		Weapon->SetOwner(Cast<AActor>(this));
		Weapon->SetInstigator(Cast<APawn>(this));

		Weapon->OnEquipped();
	}
}

ABaseWeapon* ABaseCharacter::GetWeapon() const
{
	return Weapon;
}

int ABaseCharacter::GetWeaponIndex() const
{
	return WeaponIndex;
}

void ABaseCharacter::SetIsIdle(bool value)
{
	isIdle = value;
}

bool ABaseCharacter::GetIsIdle() const
{
	return isIdle;
}

void ABaseCharacter::SetIsJumping(bool value)
{
	isJumping = value;
}

bool ABaseCharacter::GetIsJumping() const
{
	return isJumping;
}

void ABaseCharacter::ShakeCamera()
{
	if (CS_Attack) {
		PlayerController->ClientStartCameraShake(CS_Attack);
	}
}

void ABaseCharacter::SetIsInteractingToTrue()
{
	isInteracting = true;
}

void ABaseCharacter::SetIsInteractingToFalse()
{
	isInteracting = false;
}

bool ABaseCharacter::GetIsInteracting() const
{
	return isInteracting;
}

void ABaseCharacter::IncreaseHp()
{
	UP_GameInstance* GameInstance = Cast<UP_GameInstance>(GetGameInstance());
	float amount = GameInstance->CalculateHpAmount(crystalCount[0], crystalCount[1], crystalCount[2]);

	currentHp += amount;
	if (currentHp > maxHp) {
		currentHp = maxHp;
	}
	PlayerController->GetWidget()->UpdateHp(this);
}

float ABaseCharacter::GetHp() const
{
	return currentHp;
}

float ABaseCharacter::GetMaxHp() const
{
	return maxHp;
}

float ABaseCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float finalDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	currentHp -= DamageAmount;
	PlayerController->GetWidget()->UpdateHp(this);
	UE_LOG(LogTemp, Warning, TEXT("%s!\tdamage: %f, current hp: %f"), *GetName(), DamageAmount, currentHp);

	if (isIdle) {
		// 공격 중이 아닐 때
		PlayMontage(HitMontage);
	}

	if (currentHp <= 0 && this) {
		// TODO: Die 처리
	}

	return DamageAmount;
}

int ABaseCharacter::GetCrystalCount(int index) const
{
	if (index < 0 || index > 2) {
		return -1;
	}
	
	return crystalCount[index];
}

void ABaseCharacter::SaveCombo()
{
	int maxComboCount = 0;
	if (WeaponIndex == 0) {
		maxComboCount = 2;
	}
	else if (WeaponIndex == 1) {
		maxComboCount = 3;
	}
	else if (WeaponIndex == 2) {
		maxComboCount = 4;
	}

	if (comboUpdate) {
		comboUpdate = false;
		comboCount += 1;

		if (comboCount > maxComboCount) {
			return;
		}

		if (comboCount > 0) {
			BasicAttack();
		}
	}

}

void ABaseCharacter::ResetCombo()
{
	comboCount = 0;
}

void ABaseCharacter::IncreaseHitCount()
{
	++hitCount;

	if (hitCount < 10) {
		SetRank(0);
	}
	else if (hitCount < 20) {
		SetRank(1);
	}
	else if (hitCount < 30) {
		SetRank(2);
	}
	else if (hitCount < 40) {
		SetRank(3);
	}
	else if (hitCount < 50) {
		SetRank(4);
	}
	else if (hitCount < 60) {
		SetRank(5);
	}
	else if (hitCount < 70) {
		SetRank(6);
	}
	else if (hitCount < 80) {
		SetRank(7);
	}
}

int ABaseCharacter::GetHitCount() const
{
	return hitCount;
}

void ABaseCharacter::SetRank(int value)
{
	if (value >= 0 && value <= 7) {
		rank = value;
		PlayerController->GetWidget()->UpdateRank(this);
	}
}

int ABaseCharacter::GetRank() const
{
	return rank;
}

void ABaseCharacter::AddHint_Stage1(TCHAR value)
{
	Hint_Stage1.Add(value);
	PlayerController->GetWidget()->UpdateHint(this);
}

FText ABaseCharacter::GetHint_Stage1() const
{
	FText ToText;

	FString ToString;
	for (const TCHAR& hint : Hint_Stage1) {
		ToString.AppendChar(hint);
	}

	ToText = FText::FromString(ToString);
	return ToText;
}

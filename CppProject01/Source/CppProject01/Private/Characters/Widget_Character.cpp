// Fill out your copyright notice in the Description page of Project Settings.


#include "Characters/Widget_Character.h"

#include "Characters/BaseCharacter.h"
#include "Components/ProgressBar.h"
#include "Components/TextBlock.h"
#include "Components/Image.h"
#include "Animation/WidgetAnimation.h"

UWidget_Character::UWidget_Character(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	ConstructorHelpers::FObjectFinder<UTexture2D>Texture0Asset(TEXT("/Game/Blueprints/Characters/UI/WeaponFire"));
	if (Texture0Asset.Succeeded()) {
		WeaponTextures.Add(Texture0Asset.Object);
	}
	ConstructorHelpers::FObjectFinder<UTexture2D>Texture1Asset(TEXT("/Game/Blueprints/Characters/UI/WeaponIce"));
	if (Texture1Asset.Succeeded()) {
		WeaponTextures.Add(Texture1Asset.Object);
	}
	ConstructorHelpers::FObjectFinder<UTexture2D>Texture2Asset(TEXT("/Game/Blueprints/Characters/UI/WeaponElec"));
	if (Texture2Asset.Succeeded()) {
		WeaponTextures.Add(Texture2Asset.Object);
	}

	ConstructorHelpers::FObjectFinder<UTexture2D>TextureFAsset(TEXT("/Game/Blueprints/Characters/UI/RankF"));
	if (TextureFAsset.Succeeded()) {
		RankTextures.Add(TextureFAsset.Object);
	}
	ConstructorHelpers::FObjectFinder<UTexture2D>TextureDAsset(TEXT("/Game/Blueprints/Characters/UI/RankD"));
	if (TextureDAsset.Succeeded()) {
		RankTextures.Add(TextureDAsset.Object);
	}
	ConstructorHelpers::FObjectFinder<UTexture2D>TextureCAsset(TEXT("/Game/Blueprints/Characters/UI/RankC"));
	if (TextureCAsset.Succeeded()) {
		RankTextures.Add(TextureCAsset.Object);
	}
	ConstructorHelpers::FObjectFinder<UTexture2D>TextureBAsset(TEXT("/Game/Blueprints/Characters/UI/RankB"));
	if (TextureBAsset.Succeeded()) {
		RankTextures.Add(TextureBAsset.Object);
	}
	ConstructorHelpers::FObjectFinder<UTexture2D>TextureAAsset(TEXT("/Game/Blueprints/Characters/UI/RankA"));
	if (TextureAAsset.Succeeded()) {
		RankTextures.Add(TextureAAsset.Object);
	}
	ConstructorHelpers::FObjectFinder<UTexture2D>TextureSAsset(TEXT("/Game/Blueprints/Characters/UI/RankS"));
	if (TextureSAsset.Succeeded()) {
		RankTextures.Add(TextureSAsset.Object);
	}
	ConstructorHelpers::FObjectFinder<UTexture2D>TextureSSAsset(TEXT("/Game/Blueprints/Characters/UI/RankSS"));
	if (TextureSSAsset.Succeeded()) {
		RankTextures.Add(TextureSSAsset.Object);
	}
	ConstructorHelpers::FObjectFinder<UTexture2D>TextureSSSAsset(TEXT("/Game/Blueprints/Characters/UI/RankSSS"));
	if (TextureSSSAsset.Succeeded()) {
		RankTextures.Add(TextureSSSAsset.Object);
	}
}

void UWidget_Character::NativeConstruct()
{
	Super::NativeConstruct();

	HpBar = Cast<UProgressBar>(GetWidgetFromName(TEXT("HpBar")));

	Hint = Cast<UTextBlock>(GetWidgetFromName(TEXT("HintText")));

	Red = Cast<UImage>(GetWidgetFromName(TEXT("RedImage")));
	Blue = Cast<UImage>(GetWidgetFromName(TEXT("BlueImage")));
	Black = Cast<UImage>(GetWidgetFromName(TEXT("BlackImage")));
	CrystalCountRed = Cast<UTextBlock>(GetWidgetFromName(TEXT("RedCount")));
	CrystalCountBlue = Cast<UTextBlock>(GetWidgetFromName(TEXT("BlueCount")));
	CrystalCountBlack = Cast<UTextBlock>(GetWidgetFromName(TEXT("BlackCount")));

	WeaponImage = Cast<UImage>(GetWidgetFromName(TEXT("WeaponImage")));

	RankImage = Cast<UImage>(GetWidgetFromName(TEXT("RankImage")));
}

void UWidget_Character::UpdateHp(ABaseCharacter* Owner)
{
	HpBar->SetPercent(Owner->GetHp() / Owner->GetMaxHp());
}

void UWidget_Character::UpdateHint(ABaseCharacter* Owner)
{
	Hint->SetText(Owner->GetHint_Stage1());
}

void UWidget_Character::UpdateCrystalCount(ABaseCharacter* Owner)
{
	CrystalCountRed->SetText(FText::AsNumber(Owner->GetCrystalCount(0)));
	CrystalCountBlue->SetText(FText::AsNumber(Owner->GetCrystalCount(1)));
	CrystalCountBlack->SetText(FText::AsNumber(Owner->GetCrystalCount(2)));
}

void UWidget_Character::UpdateWeapon(ABaseCharacter* Owner)
{
	WeaponImage->SetBrushFromTexture(WeaponTextures[Owner->GetWeaponIndex()]);
}

void UWidget_Character::UpdateRank(ABaseCharacter* Owner)
{
	RankImage->SetBrushFromTexture(RankTextures[Owner->GetRank()]);
	RankImage->SetOpacity(1.f);
	PlayAnimation(RankFadeout);
}

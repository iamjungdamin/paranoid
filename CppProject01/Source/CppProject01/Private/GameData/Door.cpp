// Fill out your copyright notice in the Description page of Project Settings.


#include "GameData/Door.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/BoxComponent.h"

#include "Characters/BaseCharacter.h"

// Sets default values
ADoor::ADoor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ItemSkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("ItemSkeletalMesh"));
	SetRootComponent(ItemSkeletalMesh);

	ItemStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ItemStaticMesh"));
	ItemStaticMesh->SetupAttachment(GetRootComponent());
	ItemStaticMesh->SetRelativeRotation(FRotator(0.f, 90.f, 0.f));
	ItemStaticMesh->bRenderCustomDepth = true;
	ItemStaticMesh->CustomDepthStencilValue = 4;

	TriggerBox = CreateDefaultSubobject<UBoxComponent>(TEXT("TriggerBox"));
	TriggerBox->SetupAttachment(ItemStaticMesh);
	TriggerBox->SetRelativeLocation(FVector(0.f, 0.f, 100.f));
	TriggerBox->SetRelativeScale3D(FVector(6.f, 10.f, 3.f));
	TriggerBox->OnComponentBeginOverlap.AddDynamic(this, &ADoor::OnOverlapBegin);
	TriggerBox->OnComponentEndOverlap.AddDynamic(this, &ADoor::OnOverlapEnd);

	ConstructorHelpers::FObjectFinder<UStaticMesh>MeshAsset(TEXT("/Game/Blueprints/GameData/Meshes/Door/Door"));
	if (MeshAsset.Succeeded()) {
		ItemStaticMesh->SetStaticMesh(MeshAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	ConstructorHelpers::FObjectFinder<UMaterial>MaterialAsset(TEXT("/Game/Blueprints/GameData/Meshes/Door/M_Metal_Rust"));
	if (MaterialAsset.Succeeded()) {
		ItemStaticMesh->SetMaterial(0, MaterialAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}
}

// Called when the game starts or when spawned
void ADoor::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ADoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (GetOwner()) {
		ABaseCharacter* NearbyCharacter = GetOwner<ABaseCharacter>();
		if (NearbyCharacter && NearbyCharacter->GetIsInteracting()) {
			Open();
			NearbyCharacter->SetIsInteractingToFalse();
		}
	}

	if (isOpening) {
		AddActorLocalOffset(FVector(0.f, 0.f, 5.f));

		if (GetActorLocation().Z > 412.f) {
			isOpening = false;
		}
	}
}

void ADoor::Open()
{
	isOpening = true;
}

void ADoor::OnOverlapBegin(UPrimitiveComponent* Comp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
}

void ADoor::OnOverlapEnd(UPrimitiveComponent* Comp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}


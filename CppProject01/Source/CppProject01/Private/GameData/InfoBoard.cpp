// Fill out your copyright notice in the Description page of Project Settings.


#include "GameData/InfoBoard.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/TextRenderComponent.h"
#include "Components/BoxComponent.h"
#include "GameData/Widget_PwStage1.h"

#include "Kismet/GameplayStatics.h"
#include "GameData/P_GameInstance.h"
#include "Characters/BaseCharacter.h"
#include "GameData/P_PlayerController.h"

// Sets default values
AInfoBoard::AInfoBoard()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ItemSkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("ItemSkeletalMesh"));
	SetRootComponent(ItemSkeletalMesh);

	ItemStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ItemStaticMesh"));
	ItemStaticMesh->SetupAttachment(GetRootComponent());
	ItemStaticMesh->bRenderCustomDepth = true;
	ItemStaticMesh->CustomDepthStencilValue = 4;

	TextRender = CreateDefaultSubobject<UTextRenderComponent>(TEXT("TextRender"));
	TextRender->SetupAttachment(ItemStaticMesh);
	TextRender->SetRelativeLocation(FVector(13.f, 0.f, 100.f));
	TextRender->SetHorizontalAlignment(EHTA_Center);
	TextRender->SetVerticalAlignment(EVRTA_TextCenter);

	TriggerBox = CreateDefaultSubobject<UBoxComponent>(TEXT("TriggerBox"));
	TriggerBox->SetupAttachment(ItemStaticMesh);
	TriggerBox->SetRelativeLocation(FVector(80.f, 0.f, 40.f));
	TriggerBox->SetRelativeScale3D(FVector(2.f, 2.f, 1.f));
	TriggerBox->OnComponentBeginOverlap.AddDynamic(this, &AInfoBoard::OnOverlapBegin);
	TriggerBox->OnComponentEndOverlap.AddDynamic(this, &AInfoBoard::OnOverlapEnd);

	ConstructorHelpers::FObjectFinder<UStaticMesh>MeshAsset(TEXT("/Game/Blueprints/GameData/Meshes/InfoBoard/SM_Board"));
	if (MeshAsset.Succeeded()) {
		ItemStaticMesh->SetStaticMesh(MeshAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	ConstructorHelpers::FObjectFinder<UMaterial>MaterialAsset(TEXT("/Game/Blueprints/GameData/Meshes/InfoBoard/M_Wood_Walnut"));
	if (MaterialAsset.Succeeded()) {
		ItemStaticMesh->SetMaterial(0, MaterialAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	ConstructorHelpers::FClassFinder<UWidget_PwStage1>WidgetAsset(TEXT("/Game/Blueprints/GameData/UI/WidgetBP_PwStage01.WidgetBP_PwStage01_C"));
	if (WidgetAsset.Succeeded()) {
		WidgetClass = WidgetAsset.Class;
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}
}

// Called when the game starts or when spawned
void AInfoBoard::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AInfoBoard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AInfoBoard::OnOverlapBegin(UPrimitiveComponent* Comp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ABaseCharacter* OverlapCharacter = Cast<ABaseCharacter>(OtherActor);
	if (OverlapCharacter) {
		AP_PlayerController* OverlapController = OverlapCharacter->GetController<AP_PlayerController>();
		Widget = CreateWidget<UWidget_PwStage1>(OverlapController, WidgetClass);
		Widget->AddToViewport();
		Widget->FocusOnPwText(OverlapController, true);
	}
}

void AInfoBoard::OnOverlapEnd(UPrimitiveComponent* Comp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	ABaseCharacter* OverlapCharacter = Cast<ABaseCharacter>(OtherActor);
	if (OverlapCharacter) {
		AP_PlayerController* OverlapController = OverlapCharacter->GetController<AP_PlayerController>();
		Widget->FocusOnPwText(OverlapController, false);
	}
}


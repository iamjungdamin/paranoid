// Fill out your copyright notice in the Description page of Project Settings.


#include "GameData/P_GameInstance.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"

UP_GameInstance::UP_GameInstance()
{
	ConstructorHelpers::FObjectFinder<UDataTable>SwordDataAsset(TEXT("/Game/Blueprints/GameData/SwordData.SwordData"));
	if (SwordDataAsset.Succeeded()) {
		SwordDataTable = SwordDataAsset.Object;
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	ConstructorHelpers::FObjectFinder<UDataTable>MonsterDataAsset(TEXT("/Game/Blueprints/GameData/MonsterData.MonsterData"));
	if (MonsterDataAsset.Succeeded()) {
		MonsterDataTable = MonsterDataAsset.Object;
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	ConstructorHelpers::FObjectFinder<UDataTable>PasswordDataAsset(TEXT("/Game/Blueprints/GameData/PasswordData.PasswordData"));
	if (PasswordDataAsset.Succeeded()) {
		PasswordDataTable = PasswordDataAsset.Object;
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}
}

void UP_GameInstance::Init()
{
	//UE_LOG(LogTemp, Warning, TEXT("fire0, 1stage: %f"), GetSwordDataTable(0, 1)->skill3);
	//UE_LOG(LogTemp, Warning, TEXT("scarecrow: %f"), GetMonsterDataTable(FName("Scarecrow"))->maxHp);

	SetPassword_Stage1();
}

FSwordData* UP_GameInstance::GetSwordDataTable(int type, int stage)
{
	int index = 1 + type + (stage - 1) * 3;
	return SwordDataTable->FindRow<FSwordData>(*FString::FromInt(index), TEXT(""));
}

FMonsterData* UP_GameInstance::GetMonsterDataTable(FName name)
{
	return MonsterDataTable->FindRow<FMonsterData>(name, TEXT(""));
}

FPasswordData* UP_GameInstance::GetPasswordDataTable(int stage)
{
	return PasswordDataTable->FindRow<FPasswordData>(*FString::FromInt(stage), TEXT(""));
}

void UP_GameInstance::SetPassword_Stage1()
{
	Password_Stage1.Empty();

	FString pw = GetPasswordDataTable(1)->pw;
	for (int i = 0; i < pw.Len(); ++i) {
		Password_Stage1.Add(pw[i], false);
	}
}

char UP_GameInstance::GetHint_Stage1()
{
	int randomIndex = FMath::RandRange(0, Password_Stage1.Num() - 1);
	auto Iter = Password_Stage1.CreateIterator();
	for (int i = 0; i < randomIndex; ++i) {
		++Iter;
	}
	char hint = Iter->Key;
	bool isOpen = Iter->Value;

	if (!isOpen) {
		Password_Stage1[hint] = true;
		return hint;
	}

	return NULL;
}

float UP_GameInstance::CalculateHpAmount(int red, int blue, int black)
{
	float score = red * 10 + blue * 50 + black * 100;
	// TODO: 계산 제대로 바꾸기 (지금은 임시)
	float amount = 500.f;

	return amount;
}

void UP_GameInstance::OpenNextLevel()
{
	FString currentLevel = GetWorld()->GetCurrentLevel()->GetOuter()->GetName();
	int index = LevelNames.Find(FName(currentLevel));
	UE_LOG(LogTemp, Error, TEXT("%i: %s"), index, *currentLevel);

	if (index < LevelNames.Num() - 1) {
		UGameplayStatics::OpenLevel(this, LevelNames[++index]);
	}
}

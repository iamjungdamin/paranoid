// Fill out your copyright notice in the Description page of Project Settings.


#include "GameData/P_GameMode.h"
#include "Characters/BaseCharacter.h"
#include "Characters/Player02Character.h"
#include "Characters/Player03Character.h"
#include "Characters/Player04Character.h"
#include "GameData/P_PlayerController.h"

AP_GameMode::AP_GameMode()
{
	DefaultPawnClass = ABaseCharacter::StaticClass();
	PlayerControllerClass = AP_PlayerController::StaticClass();
}

void AP_GameMode::PostLogin(APlayerController* NewPlayer)
{
    int playerIndex = GetNumPlayers() - 1;
    playerIndex %= 3;

    if (playerIndex == 0) {
        DefaultPawnClass = APlayer02Character::StaticClass();
    }
    else if (playerIndex == 1) {
        DefaultPawnClass = APlayer03Character::StaticClass();
    }
    else if (playerIndex == 2) {
        DefaultPawnClass = APlayer04Character::StaticClass();
    }

    Super::PostLogin(NewPlayer);
}

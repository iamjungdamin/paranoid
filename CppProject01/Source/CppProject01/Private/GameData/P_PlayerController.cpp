// Fill out your copyright notice in the Description page of Project Settings.


#include "GameData/P_PlayerController.h"
#include "UObject/ConstructorHelpers.h"
#include "Characters/Widget_Character.h"
#include "Characters/BaseCharacter.h"

AP_PlayerController::AP_PlayerController()
{
	ConstructorHelpers::FClassFinder<UWidget_Character>WidgetAsset(TEXT("/Game/Blueprints/Characters/UI/WidgetBP_Character.WidgetBP_Character_C"));
	if (WidgetAsset.Succeeded()) {
		WidgetClass = WidgetAsset.Class;
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}
}

void AP_PlayerController::PostInitializeComponents()
{
	Super::PostInitializeComponents();
}

void AP_PlayerController::OnPossess(APawn* aPawn)
{
	Super::OnPossess(aPawn);
}

void AP_PlayerController::BeginPlay()
{
	Super::BeginPlay();

	Widget = CreateWidget<UWidget_Character>(this, WidgetClass);
	Widget->AddToViewport();

	Widget->UpdateHp(GetPawn<ABaseCharacter>());
}

UWidget_Character* AP_PlayerController::GetWidget() const
{
	return Widget;
}

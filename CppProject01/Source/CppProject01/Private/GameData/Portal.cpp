// Fill out your copyright notice in the Description page of Project Settings.


#include "GameData/Portal.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/BoxComponent.h"
#include "GameData/P_GameInstance.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"

// Sets default values
APortal::APortal()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ItemSkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("ItemSkeletalMesh"));
	SetRootComponent(ItemSkeletalMesh);

	ItemStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ItemStaticMesh"));
	ItemStaticMesh->SetupAttachment(GetRootComponent());
	ItemStaticMesh->bRenderCustomDepth = true;
	ItemStaticMesh->CustomDepthStencilValue = 4;

	TriggerBox = CreateDefaultSubobject<UBoxComponent>(TEXT("TriggerBox"));
	TriggerBox->SetupAttachment(ItemStaticMesh);
	TriggerBox->SetRelativeLocation(FVector(80.f, 0.f, 40.f));
	TriggerBox->SetRelativeScale3D(FVector(2.f, 2.f, 1.f));
	TriggerBox->OnComponentBeginOverlap.AddDynamic(this, &APortal::OnOverlapBegin);
	TriggerBox->OnComponentEndOverlap.AddDynamic(this, &APortal::OnOverlapEnd);

	Particle = CreateDefaultSubobject<UNiagaraComponent>(TEXT("Particle"));
	Particle->SetupAttachment(ItemStaticMesh);

	ConstructorHelpers::FObjectFinder<UNiagaraSystem>NiagaraAsset(TEXT("/Game/Blueprints/GameData/Particles/TelePort"));
	if (NiagaraAsset.Succeeded()) {
		Particle->SetAsset(NiagaraAsset.Object);
		Particle->bAutoActivate = false;
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}
}

// Called when the game starts or when spawned
void APortal::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APortal::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APortal::OnOverlapBegin(UPrimitiveComponent* Comp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	FTimerHandle TimerHandle;
	float delay = 5.f;
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, [=]()
		{
			UP_GameInstance* GameInstance = Cast<UP_GameInstance>(GetGameInstance());
			GameInstance->OpenNextLevel();
		}, delay, false);
}

void APortal::OnOverlapEnd(UPrimitiveComponent* Comp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}


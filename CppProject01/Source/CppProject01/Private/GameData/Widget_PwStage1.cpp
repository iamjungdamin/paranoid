// Fill out your copyright notice in the Description page of Project Settings.


#include "GameData/Widget_PwStage1.h"
#include "Components/EditableTextBox.h"
#include "GameData/P_PlayerController.h"

void UWidget_PwStage1::NativeConstruct()
{
	Super::NativeConstruct();

	PwText = Cast<UEditableTextBox>(GetWidgetFromName(TEXT("PwText")));
	if (PwText) {
		PwText->OnTextCommitted.AddDynamic(this, &UWidget_PwStage1::OnTextCommitted);
		PwText->OnTextChanged.AddDynamic(this, &UWidget_PwStage1::OnTextChanged);
	}
}

void UWidget_PwStage1::FocusOnPwText(AP_PlayerController* Controller, bool value)
{
	if (!Controller) {
		return;
	}

	if (value) {
		Controller->SetInputMode(FInputModeGameAndUI());
		Controller->bShowMouseCursor = true;
		//PwText->SetUserFocus(Controller);
	}
	else {
		Controller->SetInputMode(FInputModeGameOnly());
		Controller->bShowMouseCursor = false;
		RemoveFromParent();
	}
}

void UWidget_PwStage1::OnTextCommitted(const FText& Text, ETextCommit::Type Type)
{
	UE_LOG(LogTemp, Warning, TEXT("%s"), *Text.ToString());


}

void UWidget_PwStage1::OnTextChanged(const FText& Text)
{
	// TODO: �빮��
}

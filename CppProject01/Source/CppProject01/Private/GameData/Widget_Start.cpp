// Fill out your copyright notice in the Description page of Project Settings.


#include "GameData/Widget_Start.h"
#include "Components/Button.h"
#include "GameData/P_GameInstance.h"
#include "Kismet/KismetSystemLibrary.h"

void UWidget_Start::NativeConstruct()
{
	Super::NativeConstruct();

	Start = Cast<UButton>(GetWidgetFromName(TEXT("StartButton")));
	Exit = Cast<UButton>(GetWidgetFromName(TEXT("ExitButton")));

	if (Start) {
		Start->OnClicked.AddDynamic(this, &UWidget_Start::StartGame);
	}
	if (Exit) {
		Exit->OnClicked.AddDynamic(this, &UWidget_Start::ExitGame);
	}
}

void UWidget_Start::StartGame()
{
	UP_GameInstance* GameInstance = Cast<UP_GameInstance>(GetGameInstance());
	GameInstance->OpenNextLevel();
}

void UWidget_Start::ExitGame()
{
	UKismetSystemLibrary::QuitGame(GetWorld(), GetWorld()->GetFirstPlayerController(), EQuitPreference::Quit, false);
}

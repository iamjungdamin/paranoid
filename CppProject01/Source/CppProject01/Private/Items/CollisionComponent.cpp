// Fill out your copyright notice in the Description page of Project Settings.


#include "Items/CollisionComponent.h"
#include "Components/PrimitiveComponent.h"
#include "Kismet/KismetSystemLibrary.h"

#include "GameData/P_PlayerController.h"
#include "Monsters/BaseMonster.h"
#include "Characters/BaseCharacter.h"
#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UCollisionComponent::UCollisionComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UCollisionComponent::BeginPlay()
{
	Super::BeginPlay();

	auto Pawn = UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_Pawn);
	ObjectTypes.Add(Pawn);
}


// Called every frame
void UCollisionComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	
	if (isCollisionEnabled) {
		CollisionTrace();
	}
}

void UCollisionComponent::SetCollisionMeshComp(UPrimitiveComponent* value, int t)
{
	type = t;

	CollisionMeshComp = value;
	Owner = CollisionMeshComp->GetOwner();

	if (type == 0) {
		OwnerController = Owner->GetOwner()->GetInstigatorController();
		// TODO: 원래 tick마다 데미지 로드
	}
	else if (type == 1) {
		OwnerController = Owner->GetInstigatorController();
		ABaseMonster* M = Cast<ABaseMonster>(OwnerController->GetPawn());
		if (M) {
			damage = M->GetDamage();
		}
	}
	else if (type == 2) {
		OwnerController = Owner->GetOwner()->GetInstigatorController();
		// TODO: 데미지 로드
		//ABaseMonster* M = Cast<ABaseMonster>(OwnerController->GetPawn());
		//if (M) {
		//	damage = M->GetDamage();
		//}
	}
}


void UCollisionComponent::EnableCollision()
{
	isCollisionEnabled = true;
	ClearHitActors();
}

void UCollisionComponent::DisableCollision()
{
	isCollisionEnabled = false;
}

void UCollisionComponent::SetRadius(float value)
{
	radius = value;
}

float UCollisionComponent::GetRadius()
{
	return radius;
}

void UCollisionComponent::ClearHitActors()
{
	AlreadyHitActors.Empty();
}

void UCollisionComponent::CollisionTrace()
{
	StartLoc = CollisionMeshComp->GetSocketLocation("CollisionStart");
	EndLoc = CollisionMeshComp->GetSocketLocation("CollisionEnd");

	bool result = UKismetSystemLibrary::SphereTraceMultiForObjects(
		GetWorld(), StartLoc, EndLoc, radius,
		ObjectTypes, false, ActorsToIgnore,
		EDrawDebugTrace::ForOneFrame,
		HitResults,
		true,
		FLinearColor::Red, FLinearColor::Green, 1.f
	);
	
	if (!result) {
		// 충돌 없으면 리턴
		return;
	}

	for (FHitResult& h : HitResults) {
		AActor* HitActor = h.GetActor();

		if (AlreadyHitActors.Contains(HitActor)) {
			// 이미 처리됐으면 리턴
			return;
		}

		AlreadyHitActors.Add(HitActor);

		if (type == 0) {
			// 캐릭터가 공격
			ABaseMonster* HitMonster = Cast<ABaseMonster>(HitActor);
			if (HitMonster) {
				UGameplayStatics::ApplyDamage(HitMonster, 13.f, OwnerController, Owner, NULL);
				
				ABaseCharacter* C = Cast<ABaseCharacter>(OwnerController->GetPawn());
				if (C) {
					C->ShakeCamera();
					C->IncreaseHitCount();
				}
			}
		}
		else {
			// 몬스터가 공격
			ABaseCharacter* HitCharacter = Cast<ABaseCharacter>(HitActor);
			if (HitCharacter) {
				UGameplayStatics::ApplyDamage(HitCharacter, damage, OwnerController, Owner, NULL);
			}
		}
	}
}

void UCollisionComponent::AddActorsToIgnore(AActor* value)
{
	ActorsToIgnore.AddUnique(value);
}

void UCollisionComponent::RemoveActorsToIgnore(AActor* value)
{
	ActorsToIgnore.Remove(value);
}


// Fill out your copyright notice in the Description page of Project Settings.


#include "Items/TrailANS.h"
#include "Characters/BaseCharacter.h"
#include "Items/BaseWeapon.h"
#include "Items/CollisionComponent.h"

void UTrailANS::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration)
{
	ABaseCharacter* Character = Cast<ABaseCharacter>(MeshComp->GetOwner());
	if (!Character) {
		return;
	}

	ABaseWeapon* Weapon = Character->GetWeapon();
	if (!Weapon) {
		return;
	}

	Weapon->ActivateTrail(true);
}

void UTrailANS::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	ABaseCharacter* Character = Cast<ABaseCharacter>(MeshComp->GetOwner());
	if (!Character) {
		return;
	}

	ABaseWeapon* Weapon = Character->GetWeapon();
	if (!Weapon) {
		return;
	}

	Weapon->ActivateTrail(false);
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "MonsterItems/BaseAttackable.h"
#include "Items/CollisionComponent.h"

// Sets default values
ABaseAttackable::ABaseAttackable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ItemSkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("ItemSkeletalMesh"));
	SetRootComponent(ItemSkeletalMesh);

	ItemStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ItemStaticMesh"));
	ItemStaticMesh->SetupAttachment(GetRootComponent());
	ItemStaticMesh->bRenderCustomDepth = true;
	ItemStaticMesh->CustomDepthStencilValue = 2;
}

// Called when the game starts or when spawned
void ABaseAttackable::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABaseAttackable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABaseAttackable::OnEquipped()
{
	CollisionComp->SetCollisionMeshComp(ItemStaticMesh, 0);
	CollisionComp->AddActorsToIgnore(GetOwner());
}

UCollisionComponent* ABaseAttackable::GetCollisionComp() const
{
	return CollisionComp;
}

void ABaseAttackable::SetDamge(int value)
{
	damage = value;
}

float ABaseAttackable::GetDamage() const
{
	return damage;
}


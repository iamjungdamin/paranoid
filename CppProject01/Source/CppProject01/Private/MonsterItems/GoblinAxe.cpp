// Fill out your copyright notice in the Description page of Project Settings.


#include "MonsterItems/GoblinAxe.h"

AGoblinAxe::AGoblinAxe()
{
	ConstructorHelpers::FObjectFinder<UStaticMesh>MeshAsset(TEXT("/Game/Blueprints/MonsterItems/Meshes/Goblin/Axe"));
	if (MeshAsset.Succeeded()) {
		ItemStaticMesh->SetStaticMesh(MeshAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	ConstructorHelpers::FObjectFinder<UMaterial>MaterialAsset(TEXT("/Game/Blueprints/MonsterItems/Meshes/Goblin/m_Axe"));
	if (MaterialAsset.Succeeded()) {
		ItemStaticMesh->SetMaterial(0, MaterialAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}
}

void AGoblinAxe::Tick(float DeltaTime)
{
}

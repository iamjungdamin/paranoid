// Fill out your copyright notice in the Description page of Project Settings.


#include "MonsterItems/GolemPillar.h"
#include "UObject/ConstructorHelpers.h"
#include "Monsters/BaseMonster.h"

AGolemPillar::AGolemPillar()
{
	ConstructorHelpers::FObjectFinder<UStaticMesh>MeshAsset(TEXT("/Game/Blueprints/MonsterItems/Meshes/Golem/Pillar_SizeChange_re"));
	if (MeshAsset.Succeeded()) {
		ItemStaticMesh->SetStaticMesh(MeshAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	ConstructorHelpers::FObjectFinder<UMaterial>MaterialAsset(TEXT("/Game/Blueprints/MonsterItems/Meshes/Golem/m_pillar"));
	if (MaterialAsset.Succeeded()) {
		ItemStaticMesh->SetMaterial(0, MaterialAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	ItemStaticMesh->SetRelativeLocation(FVector(0.f, 0.f, -75.f));
	ItemStaticMesh->SetSimulatePhysics(true);
	ItemStaticMesh->SetMobility(EComponentMobility::Movable);
}

void AGolemPillar::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector Location = GetActorLocation();
	float ShakeAmplitude = 3.f;

	if (isPuttingUp) {	
		FVector ShakeOffset = FVector(FMath::FRandRange(-ShakeAmplitude, ShakeAmplitude), FMath::FRandRange(-ShakeAmplitude, ShakeAmplitude), 30.f);
		ItemStaticMesh->AddLocalOffset(ShakeOffset);

		if (ItemStaticMesh->GetRelativeLocation().Z > GetOwner<ABaseMonster>()->GetFeetLocation() - 75.f) {
			isPuttingUp = false;
		}
	}
	else {
		FVector ShakeOffset = FVector(FMath::FRandRange(-ShakeAmplitude, ShakeAmplitude), FMath::FRandRange(-ShakeAmplitude, ShakeAmplitude), 0.f);
		ItemStaticMesh->AddLocalOffset(ShakeOffset);
	}
}

void AGolemPillar::BeginPlay()
{
	Super::BeginPlay();

	SetLifeSpan(7.f);
}

void AGolemPillar::PutUp()
{
	isPuttingUp = true;
}

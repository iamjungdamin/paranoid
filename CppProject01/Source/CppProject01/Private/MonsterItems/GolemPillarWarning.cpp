// Fill out your copyright notice in the Description page of Project Settings.


#include "MonsterItems/GolemPillarWarning.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"

AGolemPillarWarning::AGolemPillarWarning()
{
	Particle = CreateDefaultSubobject<UNiagaraComponent>(TEXT("Particle"));
	Particle->SetupAttachment(ItemStaticMesh);

	ConstructorHelpers::FObjectFinder<UNiagaraSystem>NiagaraAsset(TEXT("/Game/Blueprints/MonsterItems/Particles/Golem/Circle/Magic_Circle"));
	if (NiagaraAsset.Succeeded()) {
		Particle->SetAsset(NiagaraAsset.Object);
		Particle->bAutoActivate = true;
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}
}

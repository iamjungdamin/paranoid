// Fill out your copyright notice in the Description page of Project Settings.


#include "MonsterItems/GolemStone.h"
#include "UObject/ConstructorHelpers.h"

AGolemStone::AGolemStone()
{
	ConstructorHelpers::FObjectFinder<UStaticMesh>MeshAsset(TEXT("/Game/Blueprints/MonsterItems/Meshes/Golem/rock"));
	if (MeshAsset.Succeeded()) {
		ItemStaticMesh->SetStaticMesh(MeshAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	ConstructorHelpers::FObjectFinder<UMaterial>MaterialAsset(TEXT("/Game/Blueprints/MonsterItems/Meshes/Golem/m_rock"));
	if (MaterialAsset.Succeeded()) {
		ItemStaticMesh->SetMaterial(0, MaterialAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	ItemStaticMesh->SetSimulatePhysics(true);
	ItemStaticMesh->SetMobility(EComponentMobility::Movable);
}

void AGolemStone::BeginPlay()
{
	Super::BeginPlay();

	SetLifeSpan(10.f);
}

void AGolemStone::Throw(FVector Direction)
{
	ItemStaticMesh->AddImpulse(Direction * 10000.f * 1000.f);
}

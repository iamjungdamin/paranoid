// Fill out your copyright notice in the Description page of Project Settings.


#include "MonsterItems/KnightSword.h"

AKnightSword::AKnightSword()
{
	ConstructorHelpers::FObjectFinder<UStaticMesh>MeshAsset(TEXT("/Game/Blueprints/MonsterItems/Meshes/Knight/SM_Sword"));
	if (MeshAsset.Succeeded()) {
		ItemStaticMesh->SetStaticMesh(MeshAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	ConstructorHelpers::FObjectFinder<UMaterial>MaterialAsset(TEXT("/Game/Blueprints/MonsterItems/Meshes/Knight/M_WeaponSet_1"));
	if (MaterialAsset.Succeeded()) {
		ItemStaticMesh->SetMaterial(0, MaterialAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}
}

void AKnightSword::Tick(float DeltaTime)
{
}

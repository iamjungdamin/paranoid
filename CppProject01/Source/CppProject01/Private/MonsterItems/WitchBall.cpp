// Fill out your copyright notice in the Description page of Project Settings.


#include "MonsterItems/WitchBall.h"

#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"

AWitchBall::AWitchBall()
{
	ConstructorHelpers::FObjectFinder<UStaticMesh>MeshAsset(TEXT("/Game/Blueprints/MonsterItems/Meshes/Witch/SM_WitchBall3"));
	if (MeshAsset.Succeeded()) {
		ItemStaticMesh->SetStaticMesh(MeshAsset.Object);
		ItemStaticMesh->SetVisibility(false);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	FireParticle = CreateDefaultSubobject<UNiagaraComponent>(TEXT("FireParticle"));
	FireParticle->SetupAttachment(ItemStaticMesh);

	GreenParticle = CreateDefaultSubobject<UNiagaraComponent>(TEXT("IceParticle"));
	GreenParticle->SetupAttachment(ItemStaticMesh);

	ConstructorHelpers::FObjectFinder<UNiagaraSystem>NiagaraFireAsset(TEXT("/Game/Blueprints/MonsterItems/Particles/Witch/FireSkill/Fire"));
	if (NiagaraFireAsset.Succeeded()) {
		FireParticle->SetAsset(NiagaraFireAsset.Object);
		FireParticle->bAutoActivate = false;
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	ConstructorHelpers::FObjectFinder<UNiagaraSystem>NiagaraGreenAsset(TEXT("/Game/Blueprints/MonsterItems/Particles/Witch/Projectile/MagicAttack"));
	if (NiagaraGreenAsset.Succeeded()) {
		GreenParticle->SetAsset(NiagaraGreenAsset.Object);
		GreenParticle->bAutoActivate = false;
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}
}

void AWitchBall::BeginPlay()
{
	SetLifeSpan(3.f);
}

void AWitchBall::Throw(int index, FVector Direction)
{
	if (index == 0) {
		FireParticle->SetActive(true);
	}
	else if (index == 1) {
		GreenParticle->SetActive(true);
	}
	
	ItemStaticMesh->SetSimulatePhysics(true);
	ItemStaticMesh->SetMobility(EComponentMobility::Movable);
	ItemStaticMesh->SetEnableGravity(false);
	ItemStaticMesh->AddImpulse(Direction * 1000.f);
}

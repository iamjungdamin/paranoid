// Fill out your copyright notice in the Description page of Project Settings.


#include "Monsters/BTDecorator_CanAttack.h"
#include "Monsters/MonsterAIController.h"
#include "BehaviorTree/BlackboardComponent.h"

#include "Characters/BaseCharacter.h"
#include "Monsters/BaseMonster.h"

UBTDecorator_CanAttack::UBTDecorator_CanAttack()
{
	NodeName = TEXT("CanAttack");
}

bool UBTDecorator_CanAttack::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	bool result = Super::CalculateRawConditionValue(OwnerComp, NodeMemory);

	APawn* ControllingPawn = OwnerComp.GetAIOwner()->GetPawn();
	if (nullptr == ControllingPawn) {
		return false;
	}
	
	ABaseMonster* ControllingCharacter = Cast<ABaseMonster>(ControllingPawn);
	if (nullptr == ControllingCharacter) {
		return false;
	}

	ABaseCharacter* Target = Cast<ABaseCharacter>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AMonsterAIController::key_Target[0]));
	if (nullptr == Target) {
		return false;
	}

	if (false == ControllingCharacter->GetIsIdle()) {
		return false;
	}

	ERangeType rangeType = ControllingCharacter->GetRangeType();
	if (rangeType == ERangeType::Narrow) {
		float attackDist = 200.f;
		result = Target->GetDistanceTo(ControllingPawn) <= attackDist;
		return result;
	}
	else if (rangeType == ERangeType::Wide) {
		float attackDist = 1500.f;
		result = Target->GetDistanceTo(ControllingPawn) <= attackDist;
		return result;
	}
	else if (rangeType == ERangeType::MegaWide) {
		float attackDist = 8000.f;
		result = Target->GetDistanceTo(ControllingPawn) <= attackDist;
		return result;
	}

	return false;
}

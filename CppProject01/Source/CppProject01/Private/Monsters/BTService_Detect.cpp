// Fill out your copyright notice in the Description page of Project Settings.


#include "Monsters/BTService_Detect.h"
#include "Monsters/MonsterAIController.h"
#include "BehaviorTree/BlackboardComponent.h"

#include "Characters/BaseCharacter.h"
#include "Monsters/BaseMonster.h"

UBTService_Detect::UBTService_Detect()
{
	NodeName = TEXT("Detect");
	Interval = 1.0f;
}

void UBTService_Detect::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	APawn* ControllingPawn = OwnerComp.GetAIOwner()->GetPawn();
	if (nullptr == ControllingPawn) {
		return;
	}

	ABaseMonster* ControllingCharacter = Cast<ABaseMonster>(ControllingPawn);
	if (nullptr == ControllingCharacter) {
		return;
	}

	bool isIdle = ControllingCharacter->GetIsIdle();
	if (!isIdle) {
		OwnerComp.GetBlackboardComponent()->SetValueAsEnum(AMonsterAIController::key_State, uint8(EState::NOTHING));
	}

	UWorld* World = ControllingPawn->GetWorld();
	if (nullptr == World) {
		return;
	}

	FVector Origin = ControllingPawn->GetActorLocation();

	float radius = 0.f;
	ERangeType rangeType = ControllingCharacter->GetRangeType();
	if (rangeType == ERangeType::Narrow) {
		radius = 500.f;
	}
	else if (rangeType == ERangeType::Wide) {
		radius = 1800.f;
	}
	else if (rangeType == ERangeType::MegaWide) {
		radius = 7000.f;
	}
	else {
		return;
	}

	// 자기 자신은 감지x
	FCollisionQueryParams CollisionQueryParam(NAME_None, false, ControllingPawn);

	TArray<FOverlapResult> OverlapResults;
	bool result = World->OverlapMultiByChannel(
		OverlapResults, Origin, FQuat::Identity,
		ECollisionChannel::ECC_EngineTraceChannel2,
		FCollisionShape::MakeSphere(radius),
		CollisionQueryParam
	);

	if (result) {
		int index = 0;

		for (auto& o : OverlapResults) {
			ABaseCharacter* Target = Cast<ABaseCharacter>(o.GetActor());
			if (Target) {
				OwnerComp.GetBlackboardComponent()->SetValueAsObject(AMonsterAIController::key_Target[index], Target);
				++index;

				OwnerComp.GetBlackboardComponent()->SetValueAsEnum(AMonsterAIController::key_State, uint8(EState::CHASE));
				//DrawDebugSphere(World, Origin, radius, 12, FColor::Green, false, 0.2f);

				if (index >= 3) {
					break;
				}
			}
		}

		if (index == 1) {
			OwnerComp.GetBlackboardComponent()->SetValueAsObject(AMonsterAIController::key_Target[1], nullptr);
			OwnerComp.GetBlackboardComponent()->SetValueAsObject(AMonsterAIController::key_Target[2], nullptr);
		}
		else if (index == 2) {
			OwnerComp.GetBlackboardComponent()->SetValueAsObject(AMonsterAIController::key_Target[2], nullptr);
		}
		else if (index == 3) {
		}
		else if (index == 0) {
			OwnerComp.GetBlackboardComponent()->SetValueAsObject(AMonsterAIController::key_Target[0], nullptr);
			OwnerComp.GetBlackboardComponent()->SetValueAsObject(AMonsterAIController::key_Target[1], nullptr);
			OwnerComp.GetBlackboardComponent()->SetValueAsObject(AMonsterAIController::key_Target[2], nullptr);
			OwnerComp.GetBlackboardComponent()->SetValueAsEnum(AMonsterAIController::key_State, uint8(EState::PATROL));
			//DrawDebugSphere(World, Origin, radius, 12, FColor::Red, false, 0.2f);
		}
	}
}

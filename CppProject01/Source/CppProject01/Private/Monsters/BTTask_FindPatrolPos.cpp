// Fill out your copyright notice in the Description page of Project Settings.


#include "Monsters/BTTask_FindPatrolPos.h"
#include "Monsters/MonsterAIController.h"
#include "BehaviorTree/BlackboardComponent.h"

#include "NavigationSystem.h"
#include "Monsters/BaseMonster.h"

UBTTask_FindPatrolPos::UBTTask_FindPatrolPos()
{
	NodeName = TEXT("FindPatrolPos");
}

EBTNodeResult::Type UBTTask_FindPatrolPos::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	APawn* ControllingPawn = OwnerComp.GetAIOwner()->GetPawn();
	if (nullptr == ControllingPawn) {
		return EBTNodeResult::Failed;
	}

	ABaseMonster* ControllingCharacter = Cast<ABaseMonster>(ControllingPawn);
	if (nullptr == ControllingCharacter) {
		return EBTNodeResult::Failed;
	}

	UNavigationSystemV1* NavSystem = UNavigationSystemV1::GetNavigationSystem(ControllingPawn->GetWorld());
	if (nullptr == NavSystem) {
		return EBTNodeResult::Failed;
	}

	FVector Origin = OwnerComp.GetBlackboardComponent()->GetValueAsVector(AMonsterAIController::key_HomePos);
	
	float radius = 0.f;
	ERangeType rangeType = ControllingCharacter->GetRangeType();
	if (rangeType == ERangeType::Narrow) {
		radius = 300.f;
	}
	else if (rangeType == ERangeType::Wide) {
		radius = 700.f;
	}
	else if (rangeType == ERangeType::MegaWide) {
		radius = 2000.f;
	}
	else {
		return EBTNodeResult::Failed;
	}

	FNavLocation NextPatrol;
	if (NavSystem->GetRandomPointInNavigableRadius(Origin, radius, NextPatrol)) {
		OwnerComp.GetBlackboardComponent()->SetValueAsVector(AMonsterAIController::key_PatrolPos, NextPatrol.Location);
		return EBTNodeResult::Succeeded;
	}

	return EBTNodeResult::Failed;
}

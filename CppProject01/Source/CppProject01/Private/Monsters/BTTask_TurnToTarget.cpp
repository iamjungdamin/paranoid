// Fill out your copyright notice in the Description page of Project Settings.


#include "Monsters/BTTask_TurnToTarget.h"
#include "Monsters/MonsterAIController.h"
#include "BehaviorTree/BlackboardComponent.h"

#include "Monsters/BaseMonster.h"
#include "Characters/BaseCharacter.h"

UBTTask_TurnToTarget::UBTTask_TurnToTarget()
{
	NodeName = TEXT("TurnToTarget");
}

EBTNodeResult::Type UBTTask_TurnToTarget::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	APawn* ControllingPawn = OwnerComp.GetAIOwner()->GetPawn();
	if (nullptr == ControllingPawn) {
		return EBTNodeResult::Failed;
	}

	ABaseMonster* ControllingCharacter = Cast<ABaseMonster>(ControllingPawn);
	if (nullptr == ControllingCharacter) {
		return EBTNodeResult::Failed;
	}

	ABaseCharacter* Target = Cast<ABaseCharacter>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AMonsterAIController::key_Target[0]));
	if (nullptr == Target) {
		return EBTNodeResult::Failed;
	}

	FVector LookVector = Target->GetActorLocation() - ControllingCharacter->GetActorLocation();
	LookVector.Z = 0.f;

	FRotator TargetRotator = FRotationMatrix::MakeFromX(LookVector).Rotator();
	ControllingCharacter->SetActorRotation(FMath::RInterpTo(ControllingCharacter->GetActorRotation(), TargetRotator, GetWorld()->GetDeltaSeconds(), 2.0f));

	return EBTNodeResult::Succeeded;
}

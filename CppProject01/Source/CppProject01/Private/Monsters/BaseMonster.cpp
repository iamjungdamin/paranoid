// Fill out your copyright notice in the Description page of Project Settings.


#include "Monsters/BaseMonster.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "UObject/ConstructorHelpers.h"

#include "Math/UnrealMathUtility.h"
#include "GameFramework/NavMovementComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Characters/BaseCharacter.h"
#include "Monsters/MonsterAIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"

#include "Kismet/GameplayStatics.h"
#include "GameData/P_GameInstance.h"
#include "Items/CollisionComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"

#include "Pickups/CrystalRed.h"
#include "Pickups/CrystalBlue.h"
#include "Pickups/CrystalBlack.h"

// Sets default values
ABaseMonster::ABaseMonster()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GetMesh()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetMesh()->bRenderCustomDepth = true;
	GetMesh()->CustomDepthStencilValue = 2;

	CollisionComp = CreateDefaultSubobject<UCollisionComponent>(TEXT("CollisionComp"));
	
	Blood = CreateDefaultSubobject<UNiagaraComponent>(TEXT("Trail"));
	Blood->SetupAttachment(GetMesh());
	Blood->SetRelativeLocation(FVector(0.f, 0.f, 120.f));

	ConstructorHelpers::FObjectFinder<UNiagaraSystem>NiagaraAsset(TEXT("/Game/Blueprints/Monsters/Particles/NS_Blood02"));
	if (NiagaraAsset.Succeeded()) {
		Blood->SetAsset(NiagaraAsset.Object);
		Blood->bAutoActivate = false;
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}
}

// Called when the game starts or when spawned
void ABaseMonster::BeginPlay()
{
	Super::BeginPlay();

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->bUseControllerDesiredRotation = false;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 480.f, 0.f);

	if (name != NAME_None) {
		SetStat(name);
	}

	CollisionComp->SetCollisionMeshComp(GetMesh(), 1);
	// TODO: Goblin, Knight 등등 2로 수정
	CollisionComp->AddActorsToIgnore(Cast<AActor>(this));
}

// Called every frame
void ABaseMonster::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ABaseMonster::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ABaseMonster::SetMontages(FString folderPath, int count, bool ragdoll)
{
	for (int i = 0; i < count; ++i) {
		FString AttackPath = folderPath + FString::Printf(TEXT("/AM_Attack%02d"), i + 1);
		ConstructorHelpers::FObjectFinder<UAnimMontage>AttackMontageAsset(*AttackPath);

		if (AttackMontageAsset.Succeeded()) {
			AttackMontages.Add(AttackMontageAsset.Object);
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
			continue;
		}
	}

	if (!ragdoll) {
		FString DiePath = folderPath + FString::Printf(TEXT("/AM_Die"));
		ConstructorHelpers::FObjectFinder<UAnimMontage>DieMontageAsset(*DiePath);

		if (DieMontageAsset.Succeeded()) {
			DieMontage = DieMontageAsset.Object;
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
		}

		FString HitPath = folderPath + FString::Printf(TEXT("/AM_Hit"));
		ConstructorHelpers::FObjectFinder<UAnimMontage>HitMontageAsset(*HitPath);

		if (HitMontageAsset.Succeeded()) {
			HitMontage = HitMontageAsset.Object;
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
		}
	}
}

UCollisionComponent* ABaseMonster::GetCollisionComp() const
{
	return CollisionComp;
}

float ABaseMonster::GetFeetLocation() const
{
	return GetMovementComponent()->GetActorFeetLocation().Z;
}

void ABaseMonster::SetIsIdle(bool value)
{
	isIdle = value;
}

bool ABaseMonster::GetIsIdle() const
{
	return isIdle;
}

ERangeType ABaseMonster::GetRangeType() const
{
	return rangeType;
}

void ABaseMonster::SetStat(FName monsterName)
{
	UP_GameInstance* GameInstance = Cast<UP_GameInstance>(GetGameInstance());
	FMonsterData* result = GameInstance->GetMonsterDataTable(monsterName);

	if (result) {
		stage = result->stage;
		type = result->type;
		maxHp = result->maxHp;
		currentHp = maxHp;
		damage = result->damage;
		maxCrystalDrop[0] = result->crystal0;
		maxCrystalDrop[1] = result->crystal1;
		maxCrystalDrop[2] = result->crystal2;
	}
}

float ABaseMonster::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float finalDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	currentHp -= DamageAmount;
	UE_LOG(LogTemp, Warning, TEXT("%s!\tdamage: %f, current hp: %f"), *GetName(), DamageAmount, currentHp);

	if (type < 2) {
		Hit();
	}

	if (Blood) {
		Blood->SetActive(true);
	}

	if (currentHp <= 0 && this) {
		Die(EventInstigator);
	}

	return DamageAmount;
}

float ABaseMonster::GetHp() const
{
	return currentHp;
}

float ABaseMonster::GetMaxHp() const
{
	return maxHp;
}

void ABaseMonster::SetTargets()
{
	Targets.Empty();

	AAIController* AIOwner = Cast<AAIController>(GetController());

	for (int i = 0; i < 3; ++i) {
		ABaseCharacter* Target = Cast<ABaseCharacter>(AIOwner->GetBlackboardComponent()->GetValueAsObject(AMonsterAIController::key_Target[i]));
		if (Target) {
			Targets.AddUnique(Target);
		}
	}

	FVector Origin = this->GetActorLocation();
	Targets.Sort([Origin](const AActor& A, const AActor& B) {
		float distA = (A.GetActorLocation() - Origin).SizeSquared();
		float distB = (B.GetActorLocation() - Origin).SizeSquared();
		return distA < distB;
		});
}

int ABaseMonster::Attack()
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	int attackIndex = FMath::RandRange(0, AttackMontages.Num() - 1);

	if (AnimInstance &&	AttackMontages.IsValidIndex(attackIndex) && AttackMontages[attackIndex]) {
		isIdle = false;
		AnimInstance->Montage_Play(AttackMontages[attackIndex]);
	}

	return attackIndex;
}

float ABaseMonster::GetDamage()
{
	return damage;
}

void ABaseMonster::Die(AController* EventInstigator)
{
	isIdle = false;
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCharacterMovement()->SetMovementMode(MOVE_None);
	AAIController* AIOwner = Cast<AAIController>(GetController());
	UBehaviorTreeComponent* BTObject = Cast<UBehaviorTreeComponent>(AIOwner->GetBrainComponent());
	if (BTObject) {
		BTObject->StopTree(EBTStopMode::Safe);
	}

	if (stage == 1 && type < 2) {
		char hint = Cast<UP_GameInstance>(GetGameInstance())->GetHint_Stage1();
		if (hint) {
			ABaseCharacter* InstigatorCharacter = Cast<ABaseCharacter>(EventInstigator->GetPawn());
			InstigatorCharacter->AddHint_Stage1(hint);
		}
	}
	else if (type == 2) {
		ABaseCharacter* InstigatorCharacter = Cast<ABaseCharacter>(EventInstigator->GetPawn());
		InstigatorCharacter->IncreaseHp();

		FTimerHandle TimerHandle;
		float delay = 5.f;
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, [=]()
			{
				InstigatorCharacter->OpenPortal();
			}, delay, false);
	}

	for (int i = 0; i < FMath::RandRange(0, maxCrystalDrop[0]); ++i) {
		FVector Location = GetActorLocation() + FVector(FMath::RandPointInCircle(100.f), 75.f);
		Location.Z = GetFeetLocation() + 100.f;
		FRotator Rotation = GetActorRotation();

		GetWorld()->SpawnActor<ACrystalRed>(Location, Rotation);
	}
	for (int i = 0; i < FMath::RandRange(0, maxCrystalDrop[1]); ++i) {
		FVector Location = GetActorLocation() + FVector(FMath::RandPointInCircle(100.f), 75.f);
		Location.Z = GetFeetLocation() + 100.f;
		FRotator Rotation = GetActorRotation();

		GetWorld()->SpawnActor<ACrystalBlue>(Location, Rotation);
	}
	for (int i = 0; i < FMath::RandRange(0, maxCrystalDrop[2]); ++i) {
		FVector Location = GetActorLocation() + FVector(FMath::RandPointInCircle(100.f), 75.f);
		Location.Z = GetFeetLocation() + 100.f;
		FRotator Rotation = GetActorRotation();

		GetWorld()->SpawnActor<ACrystalBlack>(Location, Rotation);
	}

	if (type == 0) {
		UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();

		if (AnimInstance && DieMontage) {
			isIdle = false;
			AnimInstance->Montage_Play(DieMontage);
		}
	}
	else {
		GetMesh()->SetCollisionProfileName(TEXT("Ragdoll"));
		GetMesh()->SetSimulatePhysics(true);

		FTimerHandle TimerHandle;
		float delay = 4.f;
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, [=]()
			{
				Destroy();
			}, delay, false);
	}
}

void ABaseMonster::Hit()
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();

	if (AnimInstance && HitMontage) {
		isIdle = false;
		AnimInstance->Montage_Play(HitMontage);
	}
}


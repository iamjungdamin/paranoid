// Fill out your copyright notice in the Description page of Project Settings.


#include "Monsters/Goblin.h"
#include "UObject/ConstructorHelpers.h"
#include "Monsters/MonsterAIController.h"
#include "MonsterItems/GoblinAxe.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"

AGoblin::AGoblin()
{
	ConstructorHelpers::FObjectFinder<USkeletalMesh>MeshAsset(TEXT("/Game/Blueprints/Monsters/Meshes/Goblin/Goblin_Final"));
	if (MeshAsset.Succeeded()) {
		GetMesh()->SetSkeletalMesh(MeshAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	ConstructorHelpers::FObjectFinder<UMaterial>MaterialAsset(TEXT("/Game/Blueprints/Monsters/Meshes/Goblin/Material__25"));
	if (MaterialAsset.Succeeded()) {
		GetMesh()->SetMaterial(0, MaterialAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	GetMesh()->SetRelativeScale3D(FVector(0.3f, 0.3f, 0.3f));
	GetMesh()->SetRelativeLocationAndRotation(FVector(0.f, 0.f, -90.f), FRotator(0.f, -90.f, 0.f));

	Blood->SetRelativeLocation(FVector(0.f, 0.f, 360.f));

	GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	ConstructorHelpers::FClassFinder<UAnimInstance>AnimInstance(TEXT("/Game/Blueprints/Monsters/ABP_GoblinAnim"));
	if (AnimInstance.Succeeded()) {
		GetMesh()->SetAnimInstanceClass(AnimInstance.Class);
	}

	name = "Goblin";
	FString folderPath = "/Game/Blueprints/Monsters/Animations/Goblin";
	SetMontages(folderPath, 2, false);

	AIControllerClass = AMonsterAIController::StaticClass();
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
}

void AGoblin::BeginPlay()
{
	Super::BeginPlay();

	Weapon = GetWorld()->SpawnActor<AGoblinAxe>(GetActorLocation(), GetActorRotation());
	Weapon->SetOwner(Cast<AActor>(this));
	Weapon->SetInstigator(Cast<APawn>(this));
	FAttachmentTransformRules TransformRules(EAttachmentRule::SnapToTarget, true);
	Weapon->AttachToComponent(GetMesh(), TransformRules, FName("AxeSocket"));
}

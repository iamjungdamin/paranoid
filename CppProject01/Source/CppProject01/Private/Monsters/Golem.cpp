// Fill out your copyright notice in the Description page of Project Settings.


#include "Monsters/Golem.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/CapsuleComponent.h"
#include "Monsters/GolemAIController.h"

#include "MonsterItems/GolemPillar.h"
#include "MonsterItems/GolemPillarWarning.h"
#include "MonsterItems/GolemStone.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "Monsters/Widget_Golem.h"
#include "Kismet/GameplayStatics.h"

AGolem::AGolem()
{
	ConstructorHelpers::FObjectFinder<USkeletalMesh>MeshAsset(TEXT("/Game/Blueprints/Monsters/Meshes/Golem/Golem_Apose"));
	if (MeshAsset.Succeeded()) {
		GetMesh()->SetSkeletalMesh(MeshAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	ConstructorHelpers::FObjectFinder<UMaterial>MaterialAsset(TEXT("/Game/Blueprints/Monsters/Meshes/Golem/Material__27"));
	if (MaterialAsset.Succeeded()) {
		GetMesh()->SetMaterial(0, MaterialAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	GetCapsuleComponent()->SetCapsuleHalfHeight(650.f);
	GetCapsuleComponent()->SetCapsuleRadius(550.f);

	GetMesh()->SetRelativeLocationAndRotation(FVector(0.f, 0.f, -650.f), FRotator(0.f, -90.f, 0.f));
	rangeType = ERangeType::MegaWide;

	GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	ConstructorHelpers::FClassFinder<UAnimInstance>AnimInstance(TEXT("/Game/Blueprints/Monsters/ABP_GolemAnim"));
	if (AnimInstance.Succeeded()) {
		GetMesh()->SetAnimInstanceClass(AnimInstance.Class);
	}

	name = "Golem";
	FString folderPath = "/Game/Blueprints/Monsters/Animations/Golem";
	SetMontages(folderPath, 4, true);

	AIControllerClass = AGolemAIController::StaticClass();
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

	Particle = CreateDefaultSubobject<UNiagaraComponent>(TEXT("Particle"));
	Particle->SetupAttachment(GetMesh());
	Particle->SetRelativeScale3D(FVector(10.f, 10.f, 1.f));

	ConstructorHelpers::FObjectFinder<UNiagaraSystem>NiagaraAsset(TEXT("/Game/Blueprints/MonsterItems/Particles/Golem/cloud_emiter_System"));
	if (NiagaraAsset.Succeeded()) {
		Particle->SetAsset(NiagaraAsset.Object);
		Particle->bAutoActivate = false;
	}

	//ConstructorHelpers::FClassFinder<UWidget_Golem>WidgetAsset(TEXT("/Game/Blueprints/Monsters/UI/WidgetBP_Golem.WidgetBP_Golem_C"));
	//if (WidgetAsset.Succeeded()) {
	//	WidgetClass = WidgetAsset.Class;
	//}
	//else {
	//	UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	//}

	ConstructorHelpers::FClassFinder<UCameraShakeBase>CameraShakeAttackAsset(TEXT("/Game/Blueprints/MonsterItems/CS_GolemAttack.CS_GolemAttack_C"));
	if (CameraShakeAttackAsset.Succeeded()) {
		CS_Attack = CameraShakeAttackAsset.Class;
	}
}

void AGolem::BeginPlay()
{
	Super::BeginPlay();
}

void AGolem::PutUpPillars()
{
	int count = FMath::RandRange(3, 7);
	// TODO: 페이즈에 따라 count 변화

	FVector OwnerLocation = GetActorLocation();
	const float angle = 2.f * PI / count;

	for (int i = 0; i < count; ++i) {
		FVector WarningLocation = OwnerLocation + FVector(FMath::Cos(angle * i), FMath::Sin(angle * i), 0.0f) * 1000.f;
		WarningLocation.Z = GetFeetLocation();
		FRotator WarningRotation = FRotator(0.f, 0.f, 0.f);

		AGolemPillarWarning* Warning = GetWorld()->SpawnActor<AGolemPillarWarning>(AGolemPillarWarning::StaticClass(), WarningLocation, WarningRotation);
		Warning->SetOwner(this);

		FTimerHandle TimerHandle;
		float delay = 1.1f;
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, [=]()
			{
				Warning->Destroy();

				FVector Location = WarningLocation + FVector(0.f, 0.f, GetFeetLocation() - 750.f);
				FRotator Rotation = WarningRotation;

				AGolemPillar* Pillar = GetWorld()->SpawnActor<AGolemPillar>(AGolemPillar::StaticClass(), Location, Rotation);
				Pillar->SetOwner(this);
				Pillar->PutUp();

			}, delay, false);
	}
}

void AGolem::ThrowStone()
{
	FVector Location = GetMesh()->GetSocketLocation("StoneSocket");
	FRotator Rotation = GetMesh()->GetSocketRotation("StoneSocket");
	AGolemStone* Stone = GetWorld()->SpawnActor<AGolemStone>(AGolemStone::StaticClass(), Location, Rotation);
	Stone->SetOwner(this);

	FVector TargetLocation = Targets[Targets.Num() - 1]->GetActorLocation();
	FVector TargetVector = TargetLocation - Stone->GetActorLocation();
	TargetVector.Normalize();
	TargetVector.Z += -0.005f;
	
	Stone->Throw(TargetVector);
}

int AGolem::Attack()
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	int attackIndex = FMath::RandRange(0, AttackMontages.Num() - 1);

	SetTargets();

	FVector TargetLocation = Targets[Targets.Num() - 1]->GetActorLocation();
	float dist = (TargetLocation - GetActorLocation()).SizeSquared();
	float attackDist = 4000.f;

	if (dist > attackDist * attackDist) {
		attackIndex = 1;
	}

	if (AnimInstance && AttackMontages.IsValidIndex(attackIndex) && AttackMontages[attackIndex]) {
		isIdle = false;
		AnimInstance->Montage_Play(AttackMontages[attackIndex]);
	}

	return attackIndex;
}

void AGolem::ShakeCamera()
{
	if (CS_Attack) {
		UGameplayStatics::PlayWorldCameraShake(GetWorld(), CS_Attack, GetActorLocation(), 800.f, 8000.f);
	}

	if (Particle->IsActive()) {
		Particle->SetActive(false);
	}
	Particle->SetActive(true);
}


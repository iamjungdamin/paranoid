// Fill out your copyright notice in the Description page of Project Settings.


#include "Monsters/GolemAIController.h"
#include "UObject/ConstructorHelpers.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/BlackboardComponent.h"

#include "Monsters/MonsterAIController.h"


AGolemAIController::AGolemAIController()
{
	ConstructorHelpers::FObjectFinder<UBlackboardData>BBObject(TEXT("/Game/Blueprints/Monsters/BB_Golem"));
	if (BBObject.Succeeded()) {
		BBAsset = BBObject.Object;
	}

	ConstructorHelpers::FObjectFinder<UBehaviorTree>BTObject(TEXT("/Game/Blueprints/Monsters/BT_Golem"));
	if (BTObject.Succeeded()) {
		BTAsset = BTObject.Object;
	}
}

void AGolemAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	UBlackboardComponent* BlackboardComp = GetBlackboardComponent();

	if (UseBlackboard(BBAsset, BlackboardComp)) {
		BlackboardComp->SetValueAsVector(AMonsterAIController::key_HomePos, InPawn->GetActorLocation());

		if (!RunBehaviorTree(BTAsset)) {
			UE_LOG(LogTemp, Warning, TEXT("Failed to Load BTAssets"));
		}
	}
}

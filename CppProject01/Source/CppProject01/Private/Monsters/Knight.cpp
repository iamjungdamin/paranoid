// Fill out your copyright notice in the Description page of Project Settings.


#include "Monsters/Knight.h"
#include "UObject/ConstructorHelpers.h"
#include "Monsters/MonsterAIController.h"
#include "MonsterItems/KnightSword.h"

AKnight::AKnight()
{
	ConstructorHelpers::FObjectFinder<USkeletalMesh>MeshAsset(TEXT("/Game/Blueprints/Monsters/Meshes/Knight/Knight_Final"));
	if (MeshAsset.Succeeded()) {
		GetMesh()->SetSkeletalMesh(MeshAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	ConstructorHelpers::FObjectFinder<UMaterial>MaterialAsset(TEXT("/Game/Blueprints/Monsters/Meshes/Knight/Material__25"));
	if (MaterialAsset.Succeeded()) {
		GetMesh()->SetMaterial(0, MaterialAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	GetMesh()->SetRelativeLocationAndRotation(FVector(0.f, 0.f, -90.f), FRotator(0.f, -90.f, 0.f));

	GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	ConstructorHelpers::FClassFinder<UAnimInstance>AnimInstance(TEXT("/Game/Blueprints/Monsters/ABP_KnightAnim"));
	if (AnimInstance.Succeeded()) {
		GetMesh()->SetAnimInstanceClass(AnimInstance.Class);
	}

	name = "Knight";
	FString folderPath = "/Game/Blueprints/Monsters/Animations/Knight";
	SetMontages(folderPath, 4, true);

	AIControllerClass = AMonsterAIController::StaticClass();
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
}

void AKnight::BeginPlay()
{
	Super::BeginPlay();

	Weapon = GetWorld()->SpawnActor<AKnightSword>(GetActorLocation(), GetActorRotation());
	Weapon->SetOwner(Cast<AActor>(this));
	Weapon->SetInstigator(Cast<APawn>(this));
	FAttachmentTransformRules TransformRules(EAttachmentRule::SnapToTarget, true);
	Weapon->AttachToComponent(GetMesh(), TransformRules, FName("SwordSocket"));
}

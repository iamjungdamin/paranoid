// Fill out your copyright notice in the Description page of Project Settings.


#include "Monsters/MonsterAIController.h"
#include "UObject/ConstructorHelpers.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/BlackboardComponent.h"


const FName AMonsterAIController::key_HomePos(TEXT("HomePos"));
const FName AMonsterAIController::key_PatrolPos(TEXT("PatrolPos"));
const FName AMonsterAIController::key_Target[3] = { FName(TEXT("Target")),
													FName(TEXT("Target02")), FName(TEXT("Target03")) };
const FName AMonsterAIController::key_State(TEXT("State"));

AMonsterAIController::AMonsterAIController()
{
	ConstructorHelpers::FObjectFinder<UBlackboardData>BBObject(TEXT("/Game/Blueprints/Monsters/BB_BaseMonster"));
	if (BBObject.Succeeded()) {
		BBAsset = BBObject.Object;
	}

	ConstructorHelpers::FObjectFinder<UBehaviorTree>BTObject(TEXT("/Game/Blueprints/Monsters/BT_BaseMonster"));
	if (BTObject.Succeeded()) {
		BTAsset = BTObject.Object;
	}
}

void AMonsterAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	UBlackboardComponent* BlackboardComp = GetBlackboardComponent();

	if (UseBlackboard(BBAsset, BlackboardComp)) {
		BlackboardComp->SetValueAsVector(key_HomePos, InPawn->GetActorLocation());

		if (!RunBehaviorTree(BTAsset)) {
			UE_LOG(LogTemp, Warning, TEXT("Failed to Load BTAssets"));
		}
	}
}


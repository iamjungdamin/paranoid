// Fill out your copyright notice in the Description page of Project Settings.


#include "Monsters/MonsterAnimInstance.h"
#include "Monsters/BaseMonster.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"

#include "Monsters/Golem.h"
#include "Monsters/Witch.h"

void UMonsterAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();

	Owner = Cast<ABaseMonster>(TryGetPawnOwner());
	if (Owner) {
		Movement = Owner->GetCharacterMovement();
	}
}

void UMonsterAnimInstance::NativeUpdateAnimation(float DeltaTime)
{
	Super::NativeUpdateAnimation(DeltaTime);

	if (Movement) {
		GroundSpeed = UKismetMathLibrary::VSizeXY(Movement->Velocity);
		IsFalling = Movement->IsFalling();
	}
}

void UMonsterAnimInstance::AnimNotify_SetIsIdle()
{
	Owner->SetIsIdle(true);
}

void UMonsterAnimInstance::AnimNotify_Die()
{
	if (Owner) {
		Owner->Destroy();
	}
}

void UMonsterAnimInstance::AnimNotify_Golem01()
{
	AGolem* OwnerGolem = Cast<AGolem>(Owner);
	if (OwnerGolem) {
		OwnerGolem->ShakeCamera();
		OwnerGolem->PutUpPillars();
	}
}

void UMonsterAnimInstance::AnimNotify_Golem02()
{
	AGolem* OwnerGolem = Cast<AGolem>(Owner);
	if (OwnerGolem) {
		OwnerGolem->ThrowStone();
	}
}

void UMonsterAnimInstance::AnimNotify_Golem03()
{
	AGolem* OwnerGolem = Cast<AGolem>(Owner);
	if (OwnerGolem) {
		OwnerGolem->ShakeCamera();
	}
}

void UMonsterAnimInstance::AnimNotify_Golem04()
{
	AGolem* OwnerGolem = Cast<AGolem>(Owner);
	if (OwnerGolem) {
		OwnerGolem->ShakeCamera();
	}
}

void UMonsterAnimInstance::AnimNotify_Witch01()
{
	AWitch* OwnerWitch = Cast<AWitch>(Owner);
	if (OwnerWitch) {
		OwnerWitch->PlayMagic(1);
	}
}

void UMonsterAnimInstance::AnimNotify_Witch02()
{
	AWitch* OwnerWitch = Cast<AWitch>(Owner);
	if (OwnerWitch) {
		OwnerWitch->PlayMagic(0);
	}
}

void UMonsterAnimInstance::AnimNotify_Witch03()
{
	AWitch* OwnerWitch = Cast<AWitch>(Owner);
	if (OwnerWitch) {
		OwnerWitch->PlayMagic(2);
	}
}


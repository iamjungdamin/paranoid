// Fill out your copyright notice in the Description page of Project Settings.


#include "Monsters/MonsterCollisionAns.h"
#include "Monsters/BaseMonster.h"
#include "Items/CollisionComponent.h"

void UMonsterCollisionAns::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration)
{
	ABaseMonster* Monster = Cast<ABaseMonster>(MeshComp->GetOwner());
	if (!Monster) {
		return;
	}

	UCollisionComponent* CollisionComp = Monster->GetCollisionComp();
	if (CollisionComp) {
		CollisionComp->EnableCollision();
	}
}

void UMonsterCollisionAns::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	ABaseMonster* Monster = Cast<ABaseMonster>(MeshComp->GetOwner());
	if (!Monster) {
		return;
	}

	UCollisionComponent* CollisionComp = Monster->GetCollisionComp();
	if (CollisionComp) {
		CollisionComp->DisableCollision();
	}
}

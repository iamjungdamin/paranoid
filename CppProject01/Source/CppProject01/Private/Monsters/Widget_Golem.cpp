// Fill out your copyright notice in the Description page of Project Settings.


#include "Monsters/Widget_Golem.h"

#include "Monsters/BaseMonster.h"
#include "Components/ProgressBar.h"

void UWidget_Golem::NativeConstruct()
{
	Super::NativeConstruct();

	HpBar = Cast<UProgressBar>(GetWidgetFromName(TEXT("HpBar")));
}

void UWidget_Golem::UpdateHp(ABaseMonster* Owner)
{
	HpBar->SetPercent(Owner->GetHp() / Owner->GetMaxHp());
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "Monsters/Widget_Witch.h"

#include "Monsters/BaseMonster.h"
#include "Components/ProgressBar.h"

void UWidget_Witch::NativeConstruct()
{
	Super::NativeConstruct();

	HpBar = Cast<UProgressBar>(GetWidgetFromName(TEXT("HpBar")));
}

void UWidget_Witch::UpdateHp(ABaseMonster* Owner)
{
	HpBar->SetPercent(Owner->GetHp() / Owner->GetMaxHp());
}


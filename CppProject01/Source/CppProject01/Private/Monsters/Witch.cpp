// Fill out your copyright notice in the Description page of Project Settings.


#include "Monsters/Witch.h"
#include "UObject/ConstructorHelpers.h"
#include "Monsters/MonsterAIController.h"

#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "MonsterItems/WitchBall.h"
#include "Monsters/Widget_Witch.h"

AWitch::AWitch()
{
	ConstructorHelpers::FObjectFinder<USkeletalMesh>MeshAsset(TEXT("/Game/Blueprints/Monsters/Meshes/Witch/Magician"));
	if (MeshAsset.Succeeded()) {
		GetMesh()->SetSkeletalMesh(MeshAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	ConstructorHelpers::FObjectFinder<UMaterial>MaterialAsset(TEXT("/Game/Blueprints/Monsters/Meshes/Witch/Material__25"));
	if (MaterialAsset.Succeeded()) {
		GetMesh()->SetMaterial(0, MaterialAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}
	
	GetMesh()->SetWorldScale3D(FVector(2.f, 2.f, 2.f));
	GetMesh()->SetRelativeLocationAndRotation(FVector(0.f, 0.f, -90.f), FRotator(0.f, -90.f, 0.f));
	rangeType = ERangeType::MegaWide;

	Blood->SetRelativeLocation(FVector(0.f, 0.f, 90.f));

	GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	ConstructorHelpers::FClassFinder<UAnimInstance>AnimInstance(TEXT("/Game/Blueprints/Monsters/ABP_WitchAnim"));
	if (AnimInstance.Succeeded()) {
		GetMesh()->SetAnimInstanceClass(AnimInstance.Class);
	}

	name = "Witch";
	FString folderPath = "/Game/Blueprints/Monsters/Animations/Witch";
	SetMontages(folderPath, 3, true);

	AIControllerClass = AMonsterAIController::StaticClass();
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

	ElectricParticle = CreateDefaultSubobject<UNiagaraComponent>(TEXT("ElectricParticle"));
	ElectricParticle->SetupAttachment(GetMesh());

	ConstructorHelpers::FObjectFinder<UNiagaraSystem>NiagaraAsset(TEXT("/Game/Blueprints/MonsterItems/Particles/Witch/LightingSkill/LigtingForce"));
	if (NiagaraAsset.Succeeded()) {
		ElectricParticle->SetAsset(NiagaraAsset.Object);
		ElectricParticle->bAutoActivate = false;
	}

	//ConstructorHelpers::FClassFinder<UWidget_Witch>WidgetAsset(TEXT("/Game/Blueprints/Monsters/UI/WidgetBP_Witch.WidgetBP_Witch_C"));
	//if (WidgetAsset.Succeeded()) {
	//	WidgetClass = WidgetAsset.Class;
	//}
	//else {
	//	UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	//}
}

void AWitch::BeginPlay()
{
	Super::BeginPlay();
}

void AWitch::PlayMagic(int attackIndex)
{
	if (attackIndex == 0) {
		FVector Location = GetMesh()->GetSocketLocation("BallSocket");
		FRotator Rotation = GetMesh()->GetSocketRotation("BallSocket");
		AWitchBall* Ball = GetWorld()->SpawnActor<AWitchBall>(Location, Rotation);

		FTimerHandle TimerHandle;
		float delay = 0.9f;
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, [=]()
			{
				Ball->Throw(0, GetActorForwardVector());
			}, delay, false);
	}
	else if (attackIndex == 1) {
		FVector Location = GetMesh()->GetSocketLocation("BallSocket");
		FRotator Rotation = GetMesh()->GetSocketRotation("BallSocket");
		AWitchBall* Ball = GetWorld()->SpawnActor<AWitchBall>(Location, Rotation);

		FTimerHandle TimerHandle;
		float delay = 0.9f;
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, [=]()
			{
				Ball->Throw(1, GetActorForwardVector());
			}, delay, false);
	}
	else if (attackIndex == 2) {
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), ElectricParticle->GetAsset(), GetActorLocation(), GetActorRotation());
	}
}


// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickups/BaseCrystal.h"

#include "Components/SphereComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"

ABaseCrystal::ABaseCrystal()
{
    PrimaryActorTick.bCanEverTick = true;
    
    ItemStaticMesh->SetSimulatePhysics(false);
    ItemStaticMesh->SetMobility(EComponentMobility::Movable);

    ItemSphereComponent->SetSphereRadius(75.f);

    ParticleSystem = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticleSystem"));
    ParticleSystem->SetupAttachment(ItemStaticMesh);
    ParticleSystem->bAutoActivate = false;

    //ConstructorHelpers::FObjectFinder<UParticleSystem>ParticleAsset(TEXT(""));
    //if (ParticleAsset.Succeeded()) {
    //    ParticleSystem->SetTemplate(ParticleAsset.Object);
    //}
    //else {
    //    UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
    //}
}

void ABaseCrystal::BeginPlay()
{
    Super::BeginPlay();

	StartLocation = GetActorLocation();
}

void ABaseCrystal::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    if (isAttracted) {
        FVector dir = AttracterLocation - GetActorLocation();
        SetActorLocation(GetActorLocation() + dir.GetSafeNormal() * DeltaTime * 500.f);

        float dist = FVector::Dist(GetActorLocation(), AttracterLocation);
        if (dist < 75.f) {
            Destroy();
        }
    }
    else {
        float gameTime = UGameplayStatics::GetRealTimeSeconds(GetWorld());
        FVector add = FVector(0.f, 0.f, FMath::Sin(gameTime * 3.f) * 5.f);
        SetActorLocation(StartLocation + add);
    }
}

void ABaseCrystal::SetCrystalType(int value)
{
    crystalType = value;
}

int ABaseCrystal::GetCrystalType() const
{
    return crystalType;
}

void ABaseCrystal::SetAttracterLocation(FVector value)
{
    AttracterLocation = value;
}

void ABaseCrystal::SetIsAttracted(bool value)
{
    isAttracted = value;
}

bool ABaseCrystal::GetIsAttracted() const
{
    return isAttracted;
}

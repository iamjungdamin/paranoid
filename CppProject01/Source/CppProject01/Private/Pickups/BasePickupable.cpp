// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickups/BasePickupable.h"
#include "Components/SphereComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ABasePickupable::ABasePickupable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ItemSphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("ItempSphereComponent"));
	SetRootComponent(ItemSphereComponent);

	ItemSkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("ItemSkeletalMesh"));
	ItemSkeletalMesh->SetupAttachment(GetRootComponent());

	ItemStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ItemStaticMesh"));
	ItemStaticMesh->SetupAttachment(GetRootComponent());
}

// Called when the game starts or when spawned
void ABasePickupable::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABasePickupable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


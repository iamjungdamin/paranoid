// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickups/CrystalBlack.h"

ACrystalBlack::ACrystalBlack()
{
	ConstructorHelpers::FObjectFinder<UStaticMesh>MeshAsset(TEXT("/Game/Blueprints/Pickups/Meshes/SM_Crystal"));
	if (MeshAsset.Succeeded()) {
		ItemStaticMesh->SetStaticMesh(MeshAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	ConstructorHelpers::FObjectFinder<UMaterial>MaterialAsset(TEXT("/Game/Blueprints/Pickups/Meshes/M_CrystalBlack"));
	if (MaterialAsset.Succeeded()) {
		ItemStaticMesh->SetMaterial(0, MaterialAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	SetCrystalType(2);
}

void ACrystalBlack::BeginPlay()
{
	Super::BeginPlay();
}

void ACrystalBlack::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

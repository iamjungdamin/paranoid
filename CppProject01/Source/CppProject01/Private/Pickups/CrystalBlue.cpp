// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickups/CrystalBlue.h"

ACrystalBlue::ACrystalBlue()
{
	ConstructorHelpers::FObjectFinder<UStaticMesh>MeshAsset(TEXT("/Game/Blueprints/Pickups/Meshes/SM_Crystal"));
	if (MeshAsset.Succeeded()) {
		ItemStaticMesh->SetStaticMesh(MeshAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	ConstructorHelpers::FObjectFinder<UMaterial>MaterialAsset(TEXT("/Game/Blueprints/Pickups/Meshes/M_CrystalBlue"));
	if (MaterialAsset.Succeeded()) {
		ItemStaticMesh->SetMaterial(0, MaterialAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	SetCrystalType(1);
}

void ACrystalBlue::BeginPlay()
{
	Super::BeginPlay();
}

void ACrystalBlue::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

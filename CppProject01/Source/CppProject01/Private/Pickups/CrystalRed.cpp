// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickups/CrystalRed.h"

ACrystalRed::ACrystalRed()
{
	ConstructorHelpers::FObjectFinder<UStaticMesh>MeshAsset(TEXT("/Game/Blueprints/Pickups/Meshes/SM_Crystal"));
	if (MeshAsset.Succeeded()) {
		ItemStaticMesh->SetStaticMesh(MeshAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	ConstructorHelpers::FObjectFinder<UMaterial>MaterialAsset(TEXT("/Game/Blueprints/Pickups/Meshes/M_CrystalRed"));
	if (MaterialAsset.Succeeded()) {
		ItemStaticMesh->SetMaterial(0, MaterialAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Failed to Load Assets"));
	}

	SetCrystalType(0);
}

void ACrystalRed::BeginPlay()
{
	Super::BeginPlay();
}

void ACrystalRed::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

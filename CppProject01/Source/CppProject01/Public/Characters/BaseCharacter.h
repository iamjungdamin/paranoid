// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BaseCharacter.generated.h"

class USpringArmComponent;
class UCameraComponent;
class ABaseWeapon;
class AP_PlayerController;
class UCameraShakeBase;

UCLASS()
class CPPPROJECT01_API ABaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABaseCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void MoveForward(float Value);
	void MoveRight(float Value);
	void Turn(float Value);
	void LookUp(float Value);
	void Jump() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
	UPROPERTY(VisibleAnywhere)
		USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere)
		UCameraComponent* ViewCamera;

	UPROPERTY()
		TSubclassOf<class UCameraShakeBase> CS_Attack;

	AP_PlayerController* PlayerController;

	bool isIdle = true;
	bool isJumping = false;

	float currentHp = 1000.f;
	float maxHp;

	int crystalCount[3];

	ABaseWeapon* Weapon;
	int WeaponIndex;

	int comboCount = 0;
	bool comboUpdate = false;

	bool isInteracting = false;

	int hitCount;
	int rank;

protected:
	void SetMontages(FString folderPath);

	// Animation Montages
	UPROPERTY(EditDefaultsOnly, Category = Montages)
		UAnimMontage* DashMontage;

	UPROPERTY(EditDefaultsOnly, Category = Montages)
		UAnimMontage* BasicAttackMontages[3];

	UPROPERTY(EditDefaultsOnly, Category = Montages)
		UAnimMontage* ChargedAttackMontages[3];

	UPROPERTY(EditDefaultsOnly, Category = Montages)
		UAnimMontage* SkillMontages[3];

	UPROPERTY(EditDefaultsOnly, Category = Montages)
		UAnimMontage* HitMontage;

private:
	void ChangeWeapon();

	void Dash();

	void ComboAttack();
	void BasicAttack();
	void Charge();
	void ChargedAttack();
	void Skill01();
	void Skill02();
	void Skill03();

	void PlayMontage(UAnimMontage* Montage, FName SectionName = NAME_None);

private:
	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* Comp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnOverlapEnd(UPrimitiveComponent* Comp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

public:
	void OpenPortal();

	void SetWeapon(int value);
	ABaseWeapon* GetWeapon() const;
	int GetWeaponIndex() const;

	void SetIsIdle(bool value);
	bool GetIsIdle() const;

	void SetIsJumping(bool value);
	bool GetIsJumping() const;

	void ShakeCamera();

	void SetIsInteractingToTrue();
	void SetIsInteractingToFalse();
	bool GetIsInteracting() const;

	void IncreaseHp();
	float GetHp() const;
	float GetMaxHp() const;

	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	int GetCrystalCount(int index) const;

	void SaveCombo();
	void ResetCombo();

	void IncreaseHitCount();
	int GetHitCount() const;

	void SetRank(int value);
	int GetRank() const;

private:
	TArray<TCHAR> Hint_Stage1;

public:
	void AddHint_Stage1(TCHAR value);
	FText GetHint_Stage1() const;
};


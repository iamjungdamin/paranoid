// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Widget_Character.generated.h"

class ABaseCharacter;
class UProgressBar;
class UTextBlock;
class UImage;
class UTexture2D;

/**
 * 
 */
UCLASS()
class CPPPROJECT01_API UWidget_Character : public UUserWidget
{
	GENERATED_BODY()

public:
	UWidget_Character(const FObjectInitializer& ObjectInitializer);
	
private:
	virtual void NativeConstruct() override;

public:
	void UpdateHp(ABaseCharacter* Owner);
	void UpdateHint(ABaseCharacter* Owner);
	void UpdateCrystalCount(ABaseCharacter* Owner);
	void UpdateWeapon(ABaseCharacter* Owner);
	void UpdateRank(ABaseCharacter* Owner);

private:
	UPROPERTY()
		UProgressBar* HpBar;

	UPROPERTY()
		UTextBlock* Hint;

	UPROPERTY()
		UImage* Red;
	UPROPERTY()
		UImage* Blue;
	UPROPERTY()
		UImage* Black;
	UPROPERTY()
		UTextBlock* CrystalCountRed;
	UPROPERTY()
		UTextBlock* CrystalCountBlue;
	UPROPERTY()
		UTextBlock* CrystalCountBlack;

	UPROPERTY()
		UImage* WeaponImage;
	UPROPERTY()
		TArray<UTexture2D*> WeaponTextures;

	UPROPERTY()
		UImage* RankImage;
	UPROPERTY()
		TArray<UTexture2D*> RankTextures;

private:
	UPROPERTY(meta = (BindWidgetAnim), Transient)
		UWidgetAnimation* RankFadeout;
};

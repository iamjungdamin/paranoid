// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InfoBoard.generated.h"

class UTextRenderComponent;
class UBoxComponent;
class UWidget_PwStage1;

UCLASS()
class CPPPROJECT01_API AInfoBoard : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInfoBoard();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(VisibleAnywhere)
		USkeletalMeshComponent* ItemSkeletalMesh;

	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* ItemStaticMesh;

	UPROPERTY(VisibleAnywhere)
		UTextRenderComponent* TextRender;

	UPROPERTY(VisibleAnywhere)
		UBoxComponent* TriggerBox;

	UPROPERTY(VisibleAnywhere)
		TSubclassOf<UWidget_PwStage1> WidgetClass;
	UPROPERTY(VisibleAnywhere)
		UWidget_PwStage1* Widget;

private:
	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* Comp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnOverlapEnd(UPrimitiveComponent* Comp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

};

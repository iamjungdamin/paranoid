// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Engine/DataTable.h"
#include "P_GameInstance.generated.h"

class UDataTable;

USTRUCT(BlueprintType)
struct FSwordData : public FTableRowBase
{
	GENERATED_BODY()
		
public:
	FSwordData() : type(0), stage(1), basic(0.f), charged(0.f), skill1(0.f), skill2(0.f), skill3(0.f) { };

	UPROPERTY(EditAnywhere, Category = "Data")
		int type;

	UPROPERTY(EditAnywhere, Category = "Data")
		int stage;

	UPROPERTY(EditAnywhere, Category = "Data")
		float basic;
	
	UPROPERTY(EditAnywhere, Category = "Data")
		float charged;

	UPROPERTY(EditAnywhere, Category = "Data")
		float skill1;

	UPROPERTY(EditAnywhere, Category = "Data")
		float skill2;

	UPROPERTY(EditAnywhere, Category = "Data")
		float skill3;
};

USTRUCT(BlueprintType)
struct FMonsterData : public FTableRowBase
{
	GENERATED_BODY()

public:
	FMonsterData() : stage(1), type(0), maxHp(30.f), damage(0.f), crystal0(0), crystal1(0), crystal2(0) { };

	UPROPERTY(EditAnywhere, Category = "Data")
		int stage;

	UPROPERTY(EditAnywhere, Category = "Data")
		int type;

	UPROPERTY(EditAnywhere, Category = "Data")
		float maxHp;

	UPROPERTY(EditAnywhere, Category = "Data")
		float damage;

	UPROPERTY(EditAnywhere, Category = "Data")
		int crystal0;

	UPROPERTY(EditAnywhere, Category = "Data")
		int crystal1;

	UPROPERTY(EditAnywhere, Category = "Data")
		int crystal2;
};

USTRUCT(BlueprintType)
struct FPasswordData : public FTableRowBase
{
	GENERATED_BODY()

public:
	FPasswordData() : pw("") { };

	UPROPERTY(EditAnywhere, Category = "Data")
		FString pw;
};

/**
 * 
 */
UCLASS()
class CPPPROJECT01_API UP_GameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	UP_GameInstance();

	virtual void Init() override;
	FSwordData* GetSwordDataTable(int type, int stage);
	FMonsterData* GetMonsterDataTable(FName name);
	FPasswordData* GetPasswordDataTable(int stage);

private:
	UPROPERTY()
		TArray<FName> LevelNames = { "Start", "Stage1", "Stage1_Boss", "Stage2" };

	UPROPERTY()
		UDataTable* SwordDataTable;

	UPROPERTY()
		UDataTable* MonsterDataTable;

	UPROPERTY()
		UDataTable* PasswordDataTable;

private:
	TMap<TCHAR, bool> Password_Stage1;

public:
	void SetPassword_Stage1();
	char GetHint_Stage1();

	float CalculateHpAmount(int red, int blue, int black);

	void OpenNextLevel();
};

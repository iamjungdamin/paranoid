// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "P_GameMode.generated.h"

/**
 * 
 */
UCLASS()
class CPPPROJECT01_API AP_GameMode : public AGameModeBase
{
	GENERATED_BODY()

private:
	AP_GameMode();
	
public:
	virtual void PostLogin(APlayerController* NewPlayer) override;
};

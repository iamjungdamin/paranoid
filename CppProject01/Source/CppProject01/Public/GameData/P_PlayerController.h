// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "P_PlayerController.generated.h"

class UWidget_Character;

/**
 * 
 */
UCLASS()
class CPPPROJECT01_API AP_PlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	AP_PlayerController();

	virtual void PostInitializeComponents() override;
	virtual void OnPossess(APawn* aPawn) override;

protected:
	virtual void BeginPlay() override;

public:
	UWidget_Character* GetWidget() const;

private:
	UPROPERTY(VisibleAnywhere)
		TSubclassOf<UWidget_Character> WidgetClass;
	UPROPERTY(VisibleAnywhere)
		UWidget_Character* Widget;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Widget_PwStage1.generated.h"

class UEditableTextBox;
class AP_PlayerController;

/**
 * 
 */
UCLASS()
class CPPPROJECT01_API UWidget_PwStage1 : public UUserWidget
{
	GENERATED_BODY()

private:
	virtual void NativeConstruct() override;

private:
	UPROPERTY()
		UEditableTextBox* PwText;

public:
	void FocusOnPwText(AP_PlayerController* Controller, bool value);

	UFUNCTION()
		void OnTextCommitted(const FText& Text, ETextCommit::Type Type);
	UFUNCTION()
		void OnTextChanged(const FText& Text);
};

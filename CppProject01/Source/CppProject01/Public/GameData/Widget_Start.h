// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Widget_Start.generated.h"

class UButton;

/**
 * 
 */
UCLASS()
class CPPPROJECT01_API UWidget_Start : public UUserWidget
{
	GENERATED_BODY()

private:
	virtual void NativeConstruct() override;

private:
	UPROPERTY()
		UButton* Start;
	UPROPERTY()
		UButton* Exit;

public:
	UFUNCTION()
		void StartGame();
	UFUNCTION()
		void ExitGame();

};

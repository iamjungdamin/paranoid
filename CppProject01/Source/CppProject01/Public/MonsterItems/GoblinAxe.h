// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MonsterItems/BaseAttackable.h"
#include "GoblinAxe.generated.h"

/**
 * 
 */
UCLASS()
class CPPPROJECT01_API AGoblinAxe : public ABaseAttackable
{
	GENERATED_BODY()

public:
	AGoblinAxe();
	virtual void Tick(float DeltaTime) override;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MonsterItems/BaseAttackable.h"
#include "GolemPillar.generated.h"

/**
 * 
 */
UCLASS()
class CPPPROJECT01_API AGolemPillar : public ABaseAttackable
{
	GENERATED_BODY()

public:
	AGolemPillar();
	virtual void Tick(float DeltaTime) override;

protected:
	virtual void BeginPlay() override;

private:
	bool isPuttingUp = false;

public:
	void PutUp();
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MonsterItems/BaseAttackable.h"
#include "GolemPillarWarning.generated.h"

class UNiagaraComponent;

/**
 * 
 */
UCLASS()
class CPPPROJECT01_API AGolemPillarWarning : public ABaseAttackable
{
	GENERATED_BODY()

public:
	AGolemPillarWarning();

private:
	UNiagaraComponent* Particle;
};

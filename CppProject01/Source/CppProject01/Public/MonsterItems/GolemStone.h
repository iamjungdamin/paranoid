// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MonsterItems/BaseAttackable.h"
#include "GolemStone.generated.h"

/**
 * 
 */
UCLASS()
class CPPPROJECT01_API AGolemStone : public ABaseAttackable
{
	GENERATED_BODY()

public:
	AGolemStone();

protected:
	virtual void BeginPlay() override;

public:
	void Throw(FVector Direction);
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MonsterItems/BaseAttackable.h"
#include "KnightSword.generated.h"

/**
 * 
 */
UCLASS()
class CPPPROJECT01_API AKnightSword : public ABaseAttackable
{
	GENERATED_BODY()
	
public:
	AKnightSword();
	virtual void Tick(float DeltaTime) override;
};

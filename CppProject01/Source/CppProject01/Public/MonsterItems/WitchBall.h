// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MonsterItems/BaseAttackable.h"
#include "WitchBall.generated.h"

class UNiagaraComponent;

/**
 * 
 */
UCLASS()
class CPPPROJECT01_API AWitchBall : public ABaseAttackable
{
	GENERATED_BODY()

public:
	AWitchBall();

protected:
	virtual void BeginPlay() override;

private:
	bool isThrowing = false;

	UNiagaraComponent* FireParticle;
	UNiagaraComponent* GreenParticle;

public:
	void Throw(int index, FVector Direction);
};

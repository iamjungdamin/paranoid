// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "BTService_DetectOne.generated.h"

/**
 * 
 */
UCLASS()
class CPPPROJECT01_API UBTService_DetectOne : public UBTService
{
	GENERATED_BODY()

public:
	UBTService_DetectOne();

protected:
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

};

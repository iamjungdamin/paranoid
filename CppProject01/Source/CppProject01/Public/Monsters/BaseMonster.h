// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Characters/BaseCharacter.h"
#include "BaseMonster.generated.h"

UENUM(BlueprintType)
enum class ERangeType : uint8 {
	Narrow,
	Wide,
	MegaWide
};

UENUM(BlueprintType)
enum class EState : uint8 {
	NOTHING,
	PATROL,
	CHASE,
	ATTACK
};

class ABaseCharacter;
class UCollisionComponent;
class UNiagaraComponent;

UCLASS()
class CPPPROJECT01_API ABaseMonster : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABaseMonster();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
	UPROPERTY(VisibleAnywhere)
		UCollisionComponent* CollisionComp;

protected:
	UPROPERTY(VisibleAnywhere)
		UNiagaraComponent* Blood;

protected:
	FName name = NAME_None;
	float feetLocation;

	bool isIdle = true;
	
	ERangeType rangeType = ERangeType::Narrow;

	int stage;
	int type;

	float maxHp;
	float currentHp;
	
	UPROPERTY()
		TArray<ABaseCharacter*> Targets;

	float damage;

	int maxCrystalDrop[3];

protected:
	virtual void SetMontages(FString folderPath, int count, bool ragdoll);

	UPROPERTY(EditDefaultsOnly, Category = Montages);
		TArray<UAnimMontage*> AttackMontages;

	UPROPERTY(EditDefaultsOnly, Category = Montages);
		UAnimMontage* DieMontage;

	UPROPERTY(EditDefaultsOnly, Category = Montages);
		UAnimMontage* HitMontage;

public:
	UCollisionComponent* GetCollisionComp() const;

	float GetFeetLocation() const;

	void SetIsIdle(bool value);
	bool GetIsIdle() const;

	ERangeType GetRangeType() const;

	void SetStat(FName monsterName);

	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	UFUNCTION(BlueprintCallable)
		float GetHp() const;
	UFUNCTION(BlueprintCallable)
		float GetMaxHp() const;

	void SetTargets();
	virtual int Attack();
	float GetDamage();

	void Die(AController* EventInstigator);
	void Hit();
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Monsters/BaseMonster.h"
#include "Goblin.generated.h"

class AGoblinAxe;

/**
 * 
 */
UCLASS()
class CPPPROJECT01_API AGoblin : public ABaseMonster
{
	GENERATED_BODY()
	
public:
	AGoblin();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	AGoblinAxe* Weapon;
};

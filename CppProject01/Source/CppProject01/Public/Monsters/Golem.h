// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Monsters/BaseMonster.h"
#include "Golem.generated.h"

class UNiagaraComponent;
class UWidget_Golem;

/**
 * 
 */
UCLASS()
class CPPPROJECT01_API AGolem : public ABaseMonster
{
	GENERATED_BODY()
	
public:
	AGolem();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleAnywhere)
		UNiagaraComponent* Particle;

public:
	void PutUpPillars();
	void ThrowStone();

	virtual int Attack() override;
	void ShakeCamera();

private:
	UPROPERTY(VisibleAnywhere)
		TSubclassOf<UWidget_Golem> WidgetClass;
	UPROPERTY(VisibleAnywhere)
		UWidget_Golem* Widget;
	// TODO: UI ���̱�

	UPROPERTY()
		TSubclassOf<class UCameraShakeBase> CS_Attack;
};

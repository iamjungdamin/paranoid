// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "GolemAIController.generated.h"

class UBlackboardData;
class UBehaviorTree;

/**
 * 
 */
UCLASS()
class CPPPROJECT01_API AGolemAIController : public AAIController
{
	GENERATED_BODY()
	
public:
	AGolemAIController();
	virtual void OnPossess(APawn* InPawn) override;

private:
	UPROPERTY()
		UBlackboardData* BBAsset;

	UPROPERTY()
		UBehaviorTree* BTAsset;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Monsters/BaseMonster.h"
#include "Knight.generated.h"

class AKnightSword;

/**
 * 
 */
UCLASS()
class CPPPROJECT01_API AKnight : public ABaseMonster
{
	GENERATED_BODY()

public:
	AKnight();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	AKnightSword* Weapon;
};

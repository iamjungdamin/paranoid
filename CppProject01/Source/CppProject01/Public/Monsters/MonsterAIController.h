// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "MonsterAIController.generated.h"

class UBlackboardData;
class UBehaviorTree;

/**
 * 
 */
UCLASS()
class CPPPROJECT01_API AMonsterAIController : public AAIController
{
	GENERATED_BODY()
	
public:
	AMonsterAIController();
	virtual void OnPossess(APawn* InPawn) override;

public:
	static const FName key_HomePos;
	static const FName key_PatrolPos;
	static const FName key_Target[3];
	static const FName key_State;

private:
	UPROPERTY()
		UBlackboardData* BBAsset;

	UPROPERTY()
		UBehaviorTree* BTAsset;
};

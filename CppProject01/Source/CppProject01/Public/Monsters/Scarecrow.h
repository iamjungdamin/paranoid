// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Monsters/BaseMonster.h"
#include "Scarecrow.generated.h"

/**
 * 
 */
UCLASS()
class CPPPROJECT01_API AScarecrow : public ABaseMonster
{
	GENERATED_BODY()

public:
	AScarecrow();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};

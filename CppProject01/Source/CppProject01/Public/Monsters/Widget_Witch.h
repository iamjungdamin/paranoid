// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Widget_Witch.generated.h"

class ABaseMonster;
class UProgressBar;

/**
 * 
 */
UCLASS()
class CPPPROJECT01_API UWidget_Witch : public UUserWidget
{
	GENERATED_BODY()

private:
	virtual void NativeConstruct() override;

public:
	void UpdateHp(ABaseMonster* Owner);

private:
	UPROPERTY()
		UProgressBar* HpBar;
};

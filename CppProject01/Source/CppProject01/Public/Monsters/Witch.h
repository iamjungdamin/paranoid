// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Monsters/BaseMonster.h"
#include "Witch.generated.h"

class UNiagaraComponent;
class UWidget_Witch;

/**
 * 
 */
UCLASS()
class CPPPROJECT01_API AWitch : public ABaseMonster
{
	GENERATED_BODY()
	
public:
	AWitch();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleAnywhere)
		UNiagaraComponent* ElectricParticle;

public:
	void PlayMagic(int index);

private:
	UPROPERTY(VisibleAnywhere)
		TSubclassOf<UWidget_Witch> WidgetClass;
	UPROPERTY(VisibleAnywhere)
		UWidget_Witch* Widget;
	// TODO: UI ���̱�
};

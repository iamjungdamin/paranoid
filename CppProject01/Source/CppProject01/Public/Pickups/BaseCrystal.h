// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickups/BasePickupable.h"
#include "BaseCrystal.generated.h"

class UParticleSystemComponent;

/**
 * 
 */
UCLASS()
class CPPPROJECT01_API ABaseCrystal : public ABasePickupable
{
	GENERATED_BODY()
	
public:
	ABaseCrystal();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY()
		UParticleSystemComponent* ParticleSystem;

	UPROPERTY()
		FVector StartLocation;

	int crystalType;

	FVector AttracterLocation;
	bool isAttracted;

public:
	void SetCrystalType(int value);
	int GetCrystalType() const;

	void SetAttracterLocation(FVector value);

	void SetIsAttracted(bool value);
	bool GetIsAttracted() const;
};

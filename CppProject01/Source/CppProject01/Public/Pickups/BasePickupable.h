// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BasePickupable.generated.h"

class USphereComponent;
class USkeletalMeshComponent;
class UStaticMeshComponent;

UCLASS()
class CPPPROJECT01_API ABasePickupable : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABasePickupable();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	UPROPERTY(VisibleAnywhere)
		USphereComponent* ItemSphereComponent;

	UPROPERTY(VisibleAnywhere)
		USkeletalMeshComponent* ItemSkeletalMesh;

	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* ItemStaticMesh;
};

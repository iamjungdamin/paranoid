// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickups/BaseCrystal.h"
#include "CrystalBlack.generated.h"

/**
 * 
 */
UCLASS()
class CPPPROJECT01_API ACrystalBlack : public ABaseCrystal
{
	GENERATED_BODY()

public:
	ACrystalBlack();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
